/* eslint-disable @typescript-eslint/no-var-requires */
// This code to create import map for i18n in develop environment
// In production, import map for i18n is executed in rollup.config.js
if (process.env.ENV === 'production') {
  return;
}

const fs = require('fs');
const path = require('path');
const glob = require('glob');

const packages = {
  '@silenteer/hermes/': './hermes/src/',
  '@design-system/': './helios/LIB_design-system/src/',
  '@infrastructure/': './helios/LIB_infrastructure/src/',
  '@todo/': './helios/app_todo/',
};

const i18nSourceDirs = {
  '@infrastructure': {
    publicPath: packages['@infrastructure/'],
    sourcePath: path.resolve(__dirname, '../helios/LIB_infrastructure/src'),
  },
  '@todo': {
    publicPath: packages['@todo/'],
    sourcePath: path.resolve(__dirname, '../helios/app_todo'),
  },
};

const i18nImports = {};

Object.keys(i18nSourceDirs).forEach((aliasName) => {
  const { publicPath, sourcePath } = i18nSourceDirs[aliasName];
  glob.sync(`${sourcePath}/**/*.i18n.*.ts`).forEach((file) => {
    const fileAliasPath = file
      .replace(sourcePath, aliasName)
      .replace('.ts', '.js');
    const filePublicPath = fileAliasPath.replace(`${aliasName}/`, publicPath);
    i18nImports[fileAliasPath] = filePublicPath;
  });
});

writeFile(i18nImports);

// ---------------------------------------------------- //

function writeFile(i18nImports) {
  // Import map for i18n creation run after webpack-config.3rd-libs.js
  // So importmap.json must be existing
  const filePath = './dist/importmap.json';
  let importMap = require(filePath);
  importMap = {
    ...importMap,
    imports: {
      ...importMap.imports,
      ...i18nImports,
    },
  };

  fs.writeFileSync(filePath, JSON.stringify(importMap, undefined, 2));
}
