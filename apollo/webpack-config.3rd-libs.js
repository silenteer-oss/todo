/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');
const fs = require('fs');
const { DefinePlugin } = require('webpack');

const dependencyMap = {
  ...require('../hermes/package.json').dependencies,
  ...require('../helios/package.json').dependencies,
};

const packages = Object.keys(dependencyMap);

const devPackages = {
  '@silenteer/hermes/': './hermes/src/',
  '@design-system/': './helios/LIB_design-system/src/',
  '@infrastructure/': './helios/LIB_infrastructure/src/',
};

const internalPackages = {
  ...(process.env.ENV === 'production' ? {} : devPackages),
  '@todo/': './helios/app_todo/',
};

fs.mkdirSync('./build/3rd-libs', { recursive: true });

const entries = packages.map((entry) => createEntrty(entry));

[].forEach((item) => createSubEntry(item));

const importmap = {
  imports: {
    ...internalPackages,
  },
};
entries.forEach(({ entryName }) => {
  const version = dependencyMap[entryName];
  importmap.imports[entryName] = `./3rd-libs/${entryName}.${version}.js`;
});

writeFile('./dist/importmap.json', JSON.stringify(importmap, undefined, 2));

module.exports = entries.map(({ entryName, filename, externals }) => {
  const version = dependencyMap[entryName];

  return {
    mode: 'production',
    entry: {
      [entryName]: filename,
    },
    output: {
      filename: `${entryName}.${version}.js`,
      path: path.resolve(__dirname, 'dist/3rd-libs'),
      libraryTarget: 'system',
    },
    devtool: 'hidden-source-map',
    externals,
    plugins: [
      new DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify('production'),
      }),
    ],
    resolve: {
      modules: [
        path.resolve(__dirname, '../helios/node_modules'),
        path.resolve(__dirname, '../hermes/node_modules'),
      ],
    },
  };
});

// --------------------------------------------------------- //

function createEntrty(moduleName) {
  const filename = `./build/3rd-libs/${moduleName}.js`;
  const filePath = path.resolve(process.cwd(), filename);

  writeFile(
    filePath,
    `
      export * from '${moduleName}';
      import lib from '${moduleName}';
      export default lib;`
  );

  const externals = packages.filter((e) => e !== moduleName);
  return { entryName: moduleName, filename, externals };
}

function createSubEntry({ moduleName, subModuleName }) {
  if (packages.includes(moduleName)) {
    entries.push(createEntrty(subModuleName));
    dependencyMap[subModuleName] = dependencyMap[moduleName];
  }
}

function writeFile(filePath, content) {
  const dir = path.dirname(filePath);
  fs.mkdirSync(dir, { recursive: true });
  fs.writeFileSync(filePath, content);
}
