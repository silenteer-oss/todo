#/usr/bin/sh

set -e
BUILD_DIR="$(pwd)/build/production"
echo $BUILD_DIR
# Build for reference
mkdir -p $BUILD_DIR
(yarn rebuild:production)

(cd ../hermes && $(yarn bin ttsc) -m ESNext --outDir "$BUILD_DIR/hermes")
(cd ../helios/LIB_design-system && $(yarn bin ttsc) -m ESNext --outDir "$BUILD_DIR/helios/LIB_design-system")
(cd ../helios/LIB_infrastructure && $(yarn bin ttsc) -m ESNext --outDir "$BUILD_DIR/helios/LIB_infrastructure")

(cd ../helios/app_todo && $(yarn bin ttsc) -m ESNext --outDir "$BUILD_DIR/helios/app_todo")

$(yarn bin rollup) -c --silent
