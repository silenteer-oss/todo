/* eslint-disable @typescript-eslint/no-var-requires */
const { dirname, join, resolve, relative } = require('path');
const { readdirSync, statSync, existsSync, writeFileSync } = require('fs');
const replace = require('rollup-plugin-replace');

// const { terser } = require('rollup-plugin-terser');

const aliasMapping = {
  '@silenteer/hermes': 'build/production/hermes/src',
  '@design-system': 'build/production/helios/LIB_design-system/src',
  '@infrastructure': 'build/production/helios/LIB_infrastructure/src',
  '@todo': 'build/production/helios/app_todo',
};

const getAllFirstIndex = (dir, indices) => {
  indices = indices || [];
  dir = resolve(dir);

  const indexFile = join(dir, 'index.js');
  let hasIndexFile = false;
  if (existsSync(indexFile)) {
    indices.push(indexFile);
    hasIndexFile = true;
  }

  const mainFile = join(dir, 'main.js');
  if (existsSync(mainFile)) {
    indices.push(mainFile);
  }

  const background = join(dir, 'background.js');
  if (existsSync(background)) {
    indices.push(background);
  }

  const theme = join(dir, 'theme.js');
  if (existsSync(theme)) {
    indices.push(theme);
  }

  const files = readdirSync(dir);
  const jsFiles = files
    .map((file) => join(dir, file))
    .filter((file) => statSync(file).isFile() && file.endsWith('.js'));

  jsFiles
    .filter((file) => file.endsWith('.en.js') || file.endsWith('.vi.js'))
    .forEach((file) => indices.push(file));

  const directories = files
    .map((file) => join(dir, file))
    .filter((file) => statSync(file).isDirectory());

  if (directories.length == 0 && !hasIndexFile) {
    jsFiles.forEach((file) => indices.push(file));
  } else if (
    directories.length > 0 &&
    !jsFiles.some((file) => file === 'index.js')
  ) {
    jsFiles
      .filter((file) => !file.endsWith('.en.js') && !file.endsWith('.vi.js'))
      .forEach((file) => indices.push(file));
  }

  directories.forEach((file) => getAllFirstIndex(file, indices));

  return indices;
};

const dynamicImports = Object.keys(aliasMapping).reduce((result, aliasPath) => {
  const path = aliasMapping[aliasPath];
  const dynamicFile = resolve(path, join('..', 'dynamic.json'));

  if (existsSync(dynamicFile)) {
    const dynamicList = result[aliasPath] || [];
    result[aliasPath] = dynamicList;

    const dynamicPaths = require(dynamicFile);
    dynamicPaths.forEach((entry) => {
      dynamicList.push(entry.replace(aliasPath, path));
    });
  }

  return result;
}, {});

const entries = Object.keys(aliasMapping).reduce(
  (result, aliasPath) => {
    const path = aliasMapping[aliasPath];
    const { rollups, importMap } = result;

    const dynamicImport = dynamicImports[aliasPath] || [];
    const indices = Array.from(
      new Set([...getAllFirstIndex(resolve(path)), ...dynamicImport])
    ).map((path) => relative(__dirname, path));

    indices.forEach((key) =>
      rollups.push({
        input: key,
        output: key.replace('build/production', 'dist'),
        aliasPath: key.replace(path, aliasPath),
        dir: dirname(key.replace('build/production', 'dist')),
      })
    );

    indices.forEach(
      (value) =>
        (importMap[value.replace(path, aliasPath)] = value.replace(
          'build/production',
          '.'
        ))
    );

    return result;
  },
  {
    rollups: [],
    importMap: {},
  }
);

let counter = 0;
const hashedImportMap = {};

const updateImportMap = () => {
  const currentImportMap = require('./dist/importmap.json');

  const updatedImportMap = {
    imports: {
      ...currentImportMap.imports,
      ...hashedImportMap,
    },
  };

  writeFileSync(
    resolve('./dist/importmap.json'),
    JSON.stringify(updatedImportMap, null, 2)
  );
};

const onwriteHook = (aliasPath) => ({
  name: 'checkHash',
  writeBundle: (option, bundle) => {
    counter += 1;
    hashedImportMap[aliasPath] = join(
      option.dir,
      Object.keys(bundle)[0]
    ).replace('dist', '.');

    if (counter === entries.rollups.length) {
      updateImportMap();
    }
  },
});

export default entries.rollups.map(({ aliasPath, input, output, dir }) => ({
  input,
  inlineDynamicImports: true,
  output: {
    dir,
    format: 'system',
    entryFileNames: '[name].[hash].js',
  },
  plugins: [
    onwriteHook(aliasPath, output),
    replace({
      'process.env.NODE_ENV': JSON.stringify('production'),
    }),
  ],
}));
