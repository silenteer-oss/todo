#!/usr/bin/env bash

VERSION=$1
TAG=$2
DATE=$(date +"%Y-%m-%d|%T")

VERSION_FILE="version.html"

# delete config file if it is existing
rm ${VERSION_FILE} 2> /dev/null

cat <<EOT >> ${VERSION_FILE}
<!DOCTYPE html>
<html>
<body>
<p>Version: ${VERSION}</p>
<p>Commit tag: ${TAG}</p>
<p>Date: ${DATE}</p>
</body>
</html>
EOT