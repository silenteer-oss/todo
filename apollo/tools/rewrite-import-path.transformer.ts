import { dirname, relative, resolve, join, extname } from 'path';
import ts from 'typescript';
import { existsSync, writeFileSync, mkdirSync, readFileSync } from 'fs';

const transformer = (_: ts.Program) => (context: ts.TransformationContext) => {
  const compilerOptions = context.getCompilerOptions();
  const rootDir = compilerOptions.rootDir;
  const outDir = compilerOptions.outDir || dirname(compilerOptions.outFile);

  return (sourceFile: ts.SourceFile) => {
    const sourceDir = dirname(sourceFile.fileName);
    const dynamicImports = [];

    const { baseUrl = '', paths = {} } = compilerOptions;
    paths['*'] = paths['*']?.concat('*') ?? ['*'];

    // Find app alias
    const alias = Object.keys(paths)
      .find(
        (path) =>
          compilerOptions.paths &&
          compilerOptions.paths[path] &&
          (compilerOptions.paths[path][0] === './src/*' ||
            compilerOptions.paths[path][0] === 'src/*')
      )
      ?.replace('/*', '');

    const getRealPaths = (pathValues: string[]) => {
      return pathValues.map((pathValue) => {
        if (pathValue === '*') return rootDir;
        return resolve(rootDir, pathValue.replace('/*', ''));
      });
    };

    const binds = Object.keys(paths)
      .filter((key) => paths[key].length)
      .map((key) => ({
        key: key.replace('/*', ''),
        regexp: new RegExp('^' + key.replace('*', '(.*)') + '$'),
        path:
          paths[key][0] === '*'
            ? rootDir
            : resolve(rootDir, paths[key][0].replace('/*', '')),
        paths: getRealPaths(paths[key]),
      }));

    if (!baseUrl || binds.length === 0) {
      // There is nothing we can do without baseUrl and paths specified.
      return sourceFile;
    }

    const implicitExtensions = ['.ts', '.tsx', '.d.ts'];
    const allowJs = compilerOptions.allowJs === true;
    const allowJsx =
      compilerOptions.jsx !== undefined &&
      compilerOptions.jsx !== ts.JsxEmit.None;
    const allowJson = compilerOptions.resolveJsonModule === true;
    allowJs && implicitExtensions.push('.js');
    allowJsx && implicitExtensions.push('.tsx');
    allowJs && allowJsx && implicitExtensions.push('.jsx');
    allowJson && implicitExtensions.push('.json');

    const { isDeclarationFile } = sourceFile;

    const resolver =
      typeof (context as any).getEmitResolver === 'function'
        ? (context as any).getEmitResolver()
        : undefined;

    const isRequire = (node: ts.Node): node is ts.CallExpression =>
      ts.isCallExpression(node) &&
      ts.isIdentifier(node.expression) &&
      node.expression.text === 'require' &&
      ts.isStringLiteral(node.arguments[0]) &&
      node.arguments.length === 1;

    const isAsyncImport = (node: ts.Node): node is ts.CallExpression =>
      ts.isCallExpression(node) &&
      node.expression.kind === ts.SyntaxKind.ImportKeyword &&
      ts.isStringLiteral(node.arguments[0]) &&
      node.arguments.length === 1;

    const isRelative = (s: string) => s[0] === '.';

    const fileExists = (s: string) => {
      // check for implicit extensions .ts, .dts, etc...
      for (const ext of implicitExtensions)
        if (existsSync(s + ext)) return true;
      // else if has extensions, file must exist
      if (extname(s) !== '') return existsSync(s);
      return false;
    };

    const bindModuleToFile = (moduleName: string) => {
      let realPath: string = undefined;

      if (moduleName.endsWith('.json')) {
        return undefined;
      }
      if (isRelative(moduleName)) {
        realPath = resolve(sourceDir, moduleName);
      } else {
        mainloop: for (const { key, paths, path } of binds) {
          for (const path of paths) {
            if (key === '*') {
              const fullPath = path + '/' + moduleName;
              if (fileExists(`${fullPath}`)) {
                realPath = fullPath;
                break mainloop;
              }

              if (fileExists(`${fullPath}/index`)) {
                realPath = fullPath;
                break mainloop;
              }
            }
          }

          if (moduleName.startsWith(key)) {
            const remainPath = moduleName.replace(key, '');
            realPath = path + remainPath;
            break mainloop;
          }
        }
      }

      if (realPath === undefined) {
        return undefined;
      }

      let modifiedModuleName = moduleName;

      if (fileExists(realPath)) {
        realPath = realPath + '.js';
        modifiedModuleName = moduleName + '.js';
      } else if (fileExists(`${realPath}/index`)) {
        realPath = `${realPath}/index.js`;
        modifiedModuleName = join(moduleName, '/index.js');
      }

      const relativePath = relative(sourceDir, realPath);

      return !isRelative(relativePath)
        ? `./${relativePath}`
        : modifiedModuleName;
    };

    const unpathRequireAndAsyncImport = (node: ts.CallExpression) => {
      const firstArg = node.arguments[0] as ts.StringLiteral;

      const file = bindModuleToFile(firstArg.text);

      if (!file) {
        return node;
      }

      const fileLiteral = ts.createLiteral(file);

      return ts.updateCall(node, node.expression, node.typeArguments, [
        fileLiteral,
      ]);
    };

    const unpathImportTypeNode = (node: ts.ImportTypeNode) => {
      const argument = node.argument as ts.LiteralTypeNode;
      const literal = argument.literal;

      if (!ts.isStringLiteral(literal)) {
        return node;
      }

      const file = bindModuleToFile(literal.text);

      if (!file) {
        return node;
      }

      const fileLiteral = ts.createLiteral(file);
      const fileArgument = ts.updateLiteralTypeNode(argument, fileLiteral);

      return ts.updateImportTypeNode(
        node,
        fileArgument,
        node.qualifier,
        node.typeArguments,
        node.isTypeOf
      );
    };

    const visitImportSpecifier = (
      node: ts.ImportSpecifier
    ): ts.VisitResult<ts.ImportSpecifier> => {
      return resolver.isReferencedAliasDeclaration(node) ? node : undefined;
    };

    const visitNamedImportBindings = (
      node: ts.NamedImportBindings
    ): ts.VisitResult<ts.NamedImportBindings> => {
      if (node.kind === ts.SyntaxKind.NamespaceImport) {
        return resolver.isReferencedAliasDeclaration(node) ? node : undefined;
      } else {
        const elements = ts.visitNodes(
          node.elements,
          visitImportSpecifier as any,
          ts.isImportSpecifier
        );
        return elements.some((e) => e)
          ? ts.updateNamedImports(node, elements)
          : undefined;
      }
    };

    const visitImportClause = (
      node: ts.ImportClause
    ): ts.VisitResult<ts.ImportClause> => {
      const name = resolver.isReferencedAliasDeclaration(node)
        ? node.name
        : undefined;
      const namedBindings = ts.visitNode(
        node.namedBindings,
        visitNamedImportBindings as any,
        ts.isNamedImports
      );
      return name || namedBindings
        ? ts.updateImportClause(node, name, namedBindings, node.isTypeOnly)
        : undefined;
    };

    const visitExportSpecifier = (
      node: ts.ExportSpecifier
    ): ts.VisitResult<ts.ExportSpecifier> => {
      return resolver.isValueAliasDeclaration(node) ? node : undefined;
    };

    const visitNamedExports = (
      node: ts.NamedExports
    ): ts.VisitResult<ts.NamedExports> => {
      const elements = ts.visitNodes(
        node.elements,
        visitExportSpecifier as any,
        ts.isExportSpecifier
      );

      return elements.some((e) => e)
        ? ts.updateNamedExports(node, elements)
        : undefined;
    };

    const unpathImportEqualsDeclaration = (
      node: ts.ExternalModuleReference
    ) => {
      if (!ts.isStringLiteral(node.expression)) {
        return node;
      }
      const file = bindModuleToFile(node.expression.text);
      if (!file) {
        return node;
      }
      const fileLiteral = ts.createLiteral(file);

      return ts.updateExternalModuleReference(node, fileLiteral);
    };

    const unpathImportDeclaration = (
      node: ts.ImportDeclaration
    ): ts.VisitResult<ts.Statement> => {
      if (!ts.isStringLiteral(node.moduleSpecifier)) {
        return node;
      }
      const file = bindModuleToFile(node.moduleSpecifier.text);
      if (!file) {
        return node;
      }
      const fileLiteral = ts.createLiteral(file);

      const importClause = ts.visitNode(
        node.importClause,
        visitImportClause as any,
        ts.isImportClause
      );
      return node.importClause === importClause ||
        importClause ||
        isDeclarationFile
        ? ts.updateImportDeclaration(
            node,
            node.decorators,
            node.modifiers,
            node.importClause,
            fileLiteral
          )
        : undefined;
    };

    const unpathExportDeclaration = (
      node: ts.ExportDeclaration
    ): ts.VisitResult<ts.Statement> => {
      if (!node.moduleSpecifier || !ts.isStringLiteral(node.moduleSpecifier)) {
        return node;
      }

      const file = bindModuleToFile(node.moduleSpecifier.text);
      if (!file) {
        return node;
      }
      const fileLiteral = ts.createLiteral(file);

      if (
        (!node.exportClause &&
          !compilerOptions.isolatedModules &&
          !resolver.moduleExportsSomeValue(node.moduleSpecifier)) ||
        (node.exportClause && resolver.isValueAliasDeclaration(node))
      ) {
        return ts.updateExportDeclaration(
          node,
          node.decorators,
          node.modifiers,
          node.exportClause,
          fileLiteral,
          node.isTypeOnly
        );
      }

      const exportClause = ts.visitNode(
        node.exportClause,
        visitNamedExports as any,
        ts.isNamedExports
      );

      return node.exportClause === exportClause ||
        exportClause ||
        isDeclarationFile
        ? ts.updateExportDeclaration(
            node,
            node.decorators,
            node.modifiers,
            node.exportClause,
            fileLiteral,
            node.isTypeOnly
          )
        : undefined;
    };

    const rewriteURLOfImportMeta = (node: ts.NewExpression) => {
      const currentPath = (node.arguments[0] as ts.StringLiteral).text;
      const updatePath = resolve(sourceDir, currentPath);

      let relativeToRootDir = relative(join(rootDir, 'src'), updatePath);

      if (relativeToRootDir.endsWith('.ts')) {
        relativeToRootDir.replace('.ts', '.js');
      } else if (!relativeToRootDir.endsWith('.js')) {
        relativeToRootDir += '.js';
      }

      if (!relativeToRootDir.endsWith('i18n.js')) {
        dynamicImports.push(`${alias}/${relativeToRootDir}`);
      }

      return ts.createStringLiteral(`${alias}/${relativeToRootDir}`);
    };

    const visit = (node: ts.Node): ts.VisitResult<ts.Node> => {
      if (
        !isDeclarationFile &&
        resolver &&
        ts.isExportDeclaration(node) &&
        !node.exportClause &&
        !compilerOptions.isolatedModules &&
        !resolver.moduleExportsSomeValue(node.moduleSpecifier)
      ) {
        return undefined;
      }

      if (isRequire(node) || isAsyncImport(node)) {
        return unpathRequireAndAsyncImport(node);
      }

      if (ts.isExternalModuleReference(node)) {
        return unpathImportEqualsDeclaration(node);
      }

      if (ts.isImportDeclaration(node)) {
        return unpathImportDeclaration(node);
      }

      if (ts.isExportDeclaration(node)) {
        return unpathExportDeclaration(node);
      }

      if (ts.isImportTypeNode(node)) {
        return unpathImportTypeNode(node);
      }

      if (
        ts.isNewExpression(node) &&
        ts.isIdentifier(node.expression) &&
        node.expression.escapedText === 'URL' &&
        node.arguments.length === 2 &&
        ts.isElementAccessExpression(node.arguments[1]) &&
        ts.isMetaProperty(node.arguments[1]['expression'])
      ) {
        return rewriteURLOfImportMeta(node);
      }

      return ts.visitEachChild(node, visit, context);
    };
    const result = ts.visitNode(sourceFile, visit);
    if (dynamicImports.length > 0) {
      if (!existsSync(outDir)) {
        mkdirSync(outDir, { recursive: true });
      }

      const dynamicJson = join(outDir, 'dynamic.json');
      if (existsSync(dynamicJson)) {
        const currentDynamics = JSON.parse(
          readFileSync(dynamicJson).toString()
        );
        const updated = Array.from(
          new Set([...currentDynamics, ...dynamicImports])
        );
        writeFileSync(dynamicJson, JSON.stringify(updated, null, 2));
      } else {
        writeFileSync(dynamicJson, JSON.stringify(dynamicImports, null, 2));
      }
    }

    return result;
  };
};

export default transformer;
