#!/usr/bin/env sh
set -eu

COMPOSER=${COMPOSER:-composer}

SKIPGA=${SKIPGA:-false}

ln -s "/app/helios/app_${WEB}/assets" "/app/assets"

INDEX="/app/helios/app_${WEB}/${COMPOSER}/index.html"
echo "index= ${INDEX}"

if [[ "${SKIPGA}" == "true" ]]; then
  BEGIN=$(grep -n '<!-- BEGIN GA SCRIPT -->' "${INDEX}" | cut -f1 -d:)
  END=$(grep -n '<!-- END GA SCRIPT -->' "${INDEX}" | cut -f1 -d:)

  if [[ ${BEGIN} -gt 0 -a  ${END} -gt 0 ]]
  then
     echo "Removing GA from line  ${BEGIN} to  ${END}"
     sed -i  -e "${BEGIN},${END}d" "${INDEX}"
  fi
fi

ln -s "${INDEX}" "/app/index.html"

exec "$@"