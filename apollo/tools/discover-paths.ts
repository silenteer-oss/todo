/* eslint-disable @typescript-eslint/no-var-requires */
/**
 * This is a tool to read all imports, discover if paths are used instead of an alias
 * How to use: CONFIG_FILE=../helios/app_todo/tsconfig.json npx ts-node ./tools/discover-paths.ts
 */
import * as fs from 'fs';
import * as path from 'path';
import * as chalk from 'chalk';
import * as ts from 'typescript';
import { Project } from 'ts-morph';

if (!process.env.CONFIG_FILE) {
  throw Error('Please provide CONFIG_FILE using environment variable');
}

const debug = (...messages: any[]) => {
  // console.log(chalk.redBright(...messages));
};

const warn = (...messages: any[]) => {
  console.log(chalk.yellow(...messages));
};

const tsConfigFilePath = process.env.CONFIG_FILE;
const project = new Project({
  tsConfigFilePath,
});

const tsconfigDir = path.dirname(tsConfigFilePath);

const projectPaths = project.getCompilerOptions().paths || {};
const aliases: string[] = [];
let paths: string[] = [];
const reverseAliasPath: ts.MapLike<string> = {};

const packageJSONFile = require(path.resolve(
  __dirname,
  '../../helios/package.json'
));
function isNodeModule(module: string) {
  return (
    packageJSONFile['dependencies'] && packageJSONFile['dependencies'][module]
  );
}

for (const alias of Object.keys(projectPaths)) {
  if (alias === '*') {
    paths = projectPaths[alias].map((entry) => entry.replace('/*', '/'));
    continue;
  }

  const normalizedAlias = alias.replace('/*', '');
  aliases.push(normalizedAlias);
  reverseAliasPath[projectPaths[alias][0].replace('/*', '/')] =
    normalizedAlias + '/';
}

function isExist(filePath: string) {
  const candidates = [
    filePath,
    filePath + '.js',
    filePath + '.ts',
    filePath + '.tsx',
  ];
  for (const candidate of candidates) {
    if (fs.existsSync(candidate)) {
      return candidate;
    }
  }
}

for (const sourceFile of project.getSourceFiles()) {
  const modified: ts.MapLike<string> = {};
  nextImport: for (const importDeclaration of sourceFile.getImportDeclarations()) {
    const importPath = importDeclaration.getModuleSpecifierValue();
    if (importPath.startsWith('.')) {
      debug('Ignore', importPath, "as it's relative already");
      continue nextImport;
    }

    if (isNodeModule(importPath)) {
      debug('Ignore', importPath, "as it's a node module");
      continue nextImport;
    }

    for (const alias of aliases) {
      if (importPath.startsWith(alias)) {
        debug('Ignore', importPath, "as it's using alias already");
        continue nextImport;
      }
    }

    for (const p of paths) {
      if (p.indexOf('node_modules') != -1) {
        continue;
      }
      // Try to resolve real path here
      const realPath = path.resolve(tsconfigDir, p, importPath);

      if (isExist(realPath)) {
        const modifiedPath = reverseAliasPath[p] + importPath;
        importDeclaration.setModuleSpecifier(modifiedPath);
        modified[importPath] = modifiedPath;
        continue nextImport;
      }
    }
  }

  if (Object.keys(modified).length > 0) {
    debug('Modified ', sourceFile.getFilePath());
    sourceFile.saveSync();
    Object.keys(modified).forEach((entry) =>
      warn(entry, '->', modified[entry])
    );
  }
}
