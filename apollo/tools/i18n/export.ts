import * as path from 'path';
import * as fs from 'fs';
import * as glob from 'glob';
import Types from './type';
import { SUPPORTED_LANGUAGES } from '../../../helios/LIB_infrastructure/src/i18n';

Types.i18nSourceDirs.forEach(({ namespace, sourcePath }) => {
  SUPPORTED_LANGUAGES.forEach((lang) => {
    const fileName = `${namespace.replace('@', '')}.${lang}.json`;
    const filePath = path.resolve(__dirname, `../../build/i18n/${fileName}`);
    const fileContent = {};

    glob.sync(`${sourcePath}/**/*.i18n.${lang}.ts`).forEach((file) => {
      const fileAliasPath = file
        .replace(sourcePath, namespace)
        .replace(`.i18n.${lang}.ts`, '');

      try {
        const content = require(file).default;
        fileContent[fileAliasPath] = content;
      } catch (error) {
        console.log(error);
        console.error(`Can not read file at ${file}`);
      }
    });

    if (Object.keys(fileContent).length > 0) {
      fs.mkdirSync(path.dirname(filePath), { recursive: true });
      fs.writeFileSync(filePath, JSON.stringify(fileContent, undefined, 2));
    }
  });
});
