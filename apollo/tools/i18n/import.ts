/* eslint-disable @typescript-eslint/no-var-requires */
import * as path from 'path';
import * as fs from 'fs';
import * as glob from 'glob';
import { CLIEngine } from 'eslint';
import Types from './type';
import { SUPPORTED_LANGUAGES } from '../../../helios/LIB_infrastructure/src/i18n';

const i18nDist = path.resolve(__dirname, `../../build/i18n`);
const eslintCLI = new CLIEngine({
  configFile: path.resolve(__dirname, '../../../helios/.eslintrc.js'),
  fix: true,
});

Types.i18nSourceDirs.forEach(({ namespace, sourcePath }) => {
  SUPPORTED_LANGUAGES.forEach((lang) => {
    glob
      .sync(`${i18nDist}/${namespace.replace('@', '')}.${lang}.json`)
      .forEach((file) => {
        try {
          const translatedFileContent = require(file);

          Object.keys(translatedFileContent).forEach((aliasPath) => {
            const sourceFilePath = path.resolve(
              __dirname,
              `${aliasPath.replace(namespace, sourcePath)}.i18n.${lang}.ts`
            );

            try {
              if (fs.existsSync(sourceFilePath)) {
                const sourceFileContent = {
                  ...require(sourceFilePath).default,
                  ...translatedFileContent[aliasPath],
                };

                const { results } = eslintCLI.executeOnText(
                  `export default ${JSON.stringify(
                    sourceFileContent,
                    undefined,
                    2
                  )}`
                );

                if (results[0]?.output) {
                  fs.writeFileSync(sourceFilePath, results[0].output);
                } else {
                  console.error(`Can not lint file at ${sourceFilePath}`);
                }
              } else {
                console.warn(`File is not existing at ${sourceFilePath}`);
              }
            } catch (error) {
              console.error(`Can not write file at ${sourceFilePath}`);
              console.error(error);
            }
          });
        } catch (error) {
          console.error(`Can not read file at ${file}`);
          console.error(error);
        }
      });
  });
});
