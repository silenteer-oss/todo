import * as path from 'path';

const i18nSourceDirs = [
  {
    namespace: '@infrastructure',
    sourcePath: path.resolve(
      __dirname,
      '../../../helios/LIB_infrastructure/src'
    ),
  },
  {
    namespace: '@sysadmin',
    sourcePath: path.resolve(__dirname, '../../../helios/app_sysadmin'),
  },
  {
    namespace: '@todo',
    sourcePath: path.resolve(__dirname, '../../../helios/app_admin'),
  },
  {
    namespace: '@careprovider',
    sourcePath: path.resolve(__dirname, '../../../helios/app_careprovider'),
  },
  {
    namespace: '@patient',
    sourcePath: path.resolve(__dirname, '../../../helios/app_patient'),
  },
];

export default {
  i18nSourceDirs,
};
