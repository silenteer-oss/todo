/* eslint-disable @typescript-eslint/no-var-requires */
const fs = require('fs');
const path = require('path');
const cheerio = require('cheerio');
const morgan = require('morgan');

// ----------------------------------------------------------------------- //

const logger = morgan('dev', {
  skip: (req, res) => res.statusCode < 400,
});

// ----------------------------------------------------------------------- //

const apiProxy = require('http-proxy-middleware').createProxyMiddleware(
  '/api',
  {
    target: 'http://localhost:8096',
    changeOrigin: true,
    logLevel: 'debug',
  }
);

// ----------------------------------------------------------------------- //

const socketProxy = require('http-proxy-middleware').createProxyMiddleware(
  '/ws',
  {
    target: 'http://localhost:8095',
    changeOrigin: true,
    ws: true,
    logLevel: 'debug',
  }
);

// ----------------------------------------------------------------------- //

const appNameMap = {
  'todo.local': 'app_todo',
};
// This injected code is used to reload browser for every changes
const INJECTED_CODE = fs.readFileSync(
  path.join(__dirname, './injected-live-reload.html'),
  'utf8'
);

// Used in development to dynamic generate html for applications & modules
const selfHosting = (req, res, next) => {
  try {
    const pathname = req.url;
    const hostname = new URL(`http://${req.headers['host']}`).hostname;
    const appName = appNameMap[hostname];

    if (!appName) {
      next();
      return;
    }
    if (pathname.endsWith('.map')) {
      next();
      return;
    }

    if (pathname.startsWith('/assets/')) {
      const assetPath = pathname.split('?')[0];
      const extension = assetPath.split('.').pop();
      let contentType;

      if (extension === 'json') {
        contentType = 'application/json';
      } else if (extension === 'html') {
        contentType = 'text/html';
      } else if (extension === 'css') {
        contentType = 'text/css';
      } else if (extension === 'js') {
        contentType = 'text/javascript';
      } else if (extension === 'ico') {
        contentType = 'image/vnd.microsoft.icon';
      } else if (extension === 'svg') {
        contentType = 'image/svg+xml';
      } else if (extension === 'png') {
        contentType = 'image/png';
      } else if (['jpg', 'jpeg'].includes(extension)) {
        contentType = 'image/jpeg';
      } else if (extension === 'eot') {
        contentType = 'application/vnd.ms-fontobject';
      } else if (['ttf', 'woff'].includes(extension)) {
        contentType = `font/${extension}`;
      }

      if (contentType) {
        const filePath = getFileFromAsset({
          appName,
          fileName: assetPath.replace('/assets/', ''),
          useAbsolutePath: true,
        });
        if (fs.existsSync(filePath)) {
          res.writeHead(200, { 'Content-Type': contentType });
          fs.createReadStream(filePath).pipe(res);
        } else {
          next();
        }
      } else {
        next();
      }
      return;
    }

    const $ = cheerio.load(
      fs.readFileSync(
        getFileFromApp({
          appName,
          fileName: 'index.html',
          useAbsolutePath: true,
          readFromCodebase: true,
        })
      )
    );

    if (pathname.startsWith('/module/')) {
      const moduleName = pathname.split('/').pop();
      const mainUrl = getFileFromModule({
        appName,
        moduleName,
        fileName: 'main.js',
      });

      $('#init-script').text(`
      window.require = console.log;
      window.process = { env: { NODE_ENV: 'develop' } };
      System.import('${mainUrl}');
    `);
      $('#ga-script').remove();
      $('#ga-config-script').remove();
      $('body').append(INJECTED_CODE);

      res.writeHead(200, { 'Content-Type': 'text/html' });
      res.write($.html());
      res.end();
      return;
    }

    if (req.headers['accept'] && req.headers['accept'].includes('text/html')) {
      const mainUrl = getFileFromApp({
        appName,
        fileName: 'main.js',
      });

      $('#init-script').text(`
      window.require = console.log;
      window.process = { env: { NODE_ENV: 'develop' } };
      System.import('${mainUrl}');
    `);
      $('#ga-script').remove();
      $('#ga-config-script').remove();
      $('body').append(INJECTED_CODE);

      res.writeHead(200, { 'Content-Type': 'text/html' });
      res.write($.html());
      res.end();
      return;
    }

    next();
  } catch (error) {
    console.error(error);
    next(error);
  }
};

// ----------------------------------------------------------------------- //

function getFileFromAsset({
  appName,
  fileName,
  useAbsolutePath,
  readFromCodebase,
}) {
  return useAbsolutePath
    ? path.resolve(
        __dirname,
        path.join(
          `${readFromCodebase ? '../..' : '../dist'}/helios/${appName}/assets`,
          fileName
        )
      )
    : path.join(`/helios/${appName}/assets`, fileName);
}

function getFileFromApp({
  appName,
  fileName,
  useAbsolutePath,
  readFromCodebase,
}) {
  return useAbsolutePath
    ? path.resolve(
        __dirname,
        path.join(
          `${
            readFromCodebase ? '../..' : '../dist'
          }/helios/${appName}/composer`,
          fileName
        )
      )
    : path.join(`${appName.replace('app_', '@')}/composer`, fileName);
}

function getFileFromModule({
  appName,
  moduleName,
  fileName,
  useAbsolutePath,
  readFromCodebase,
}) {
  return useAbsolutePath
    ? path.resolve(
        __dirname,
        path.join(
          `${
            readFromCodebase ? '../..' : '../dist'
          }/helios/${appName}/module_${moduleName}`,
          fileName
        )
      )
    : path.join(
        `${appName.replace('app_', '@')}/module_${moduleName}`,
        fileName
      );
}

// ----------------------------------------------------------------------- //

module.exports = {
  logger,
  apiProxy,
  socketProxy,
  selfHosting,
};
