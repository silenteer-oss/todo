/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');
const http = require('http');
const WebSocket = require('ws');
const chokidar = require('chokidar');
const express = require('express');
const { logger, selfHosting, apiProxy, socketProxy } = require('./middlewares');

const dist = path.resolve(__dirname, '../dist/');

const app = express();
app.use('/', express.static(dist));
app.use(logger, selfHosting, apiProxy, socketProxy);

const websocketServer = new WebSocket.Server({ noServer: true });

const server = http.createServer(app);
server.addListener('upgrade', (request, socket, head) => {
  if (request.url.startsWith('/ws/')) {
    socketProxy.upgrade(request, socket, head);
  } else if (request.url.startsWith('/dev-socket/')) {
    websocketServer.handleUpgrade(request, socket, head, (websocketClient) => {
      websocketServer.emit('connection', websocketClient, request);
    });
  }
});

const handleChange = debounce(1000, () => {
  if (websocketServer) {
    websocketServer.clients.forEach((client) => {
      client.send('reload');
    });
  }
});
chokidar
  .watch(dist)
  .on('change', handleChange)
  .on('add', handleChange)
  .on('unlink', handleChange)
  .on('addDir', handleChange)
  .on('unlinkDir', handleChange)
  .on('error', (error) => {
    console.log('ERROR:'.red, error);
  });

server.listen(6969, () => {
  console.log('dev server is running');
});

// ----------------------------------------------------------------------- //

function debounce(delay, originalFunc) {
  let timeout;

  return function (...args) {
    const functionCall = () => originalFunc.apply(this, args);

    clearTimeout(timeout);
    timeout = setTimeout(functionCall, delay);
  };
}
