Apollo can be considered as a build tool for app_* in hellios.
It use tsc to build source code from typescript to system module, and watch changes from source code.
In runtime, systemjs will lazy loading dependencies(includes node modules and interal packages) for each view.
Apollo also setup a node server work as a live server.

To build all projects in hellios
```
yarn build
```

To rebuild all projects in hellios, rebuild will remove dist folder first and force tsc run build from scratch again.
```
yarn rebuild
```

To watch changes from ts files in hellios and re-compile only changed files
```
yarn dev
```

To start live server
```
yarn serve
```
