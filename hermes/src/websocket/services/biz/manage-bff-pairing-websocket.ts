import { MicronautsWebSocketClient } from '../../models';

export const createBFFPairingWs = (hostUrl: string, pairingAddress: string) => new MicronautsWebSocketClient(
    `${hostUrl}/ws/device/${pairingAddress}`,
    {autoPing: false, autoReconnect: false}
);
