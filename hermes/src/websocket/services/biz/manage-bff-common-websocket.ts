import {
    MicronautsWebSocketClient,
    WebSocketResponseMessage,
} from '../../models';

const _REQUEST_TIME_OUT = 30 * 1000;

let _commonSocket: MicronautsWebSocketClient;

export const COMMON_SOCKET_OPEN_EVENT = 'COMMON_SOCKET_OPEN_EVENT';
export const COMMON_SOCKET_MESSAGE_EVENT = 'COMMON_SOCKET_MESSAGE_EVENT';
export const COMMON_SOCKET_CLOSE_EVENT = 'COMMON_SOCKET_CLOSE_EVENT';
export const COMMON_SOCKET_ERROR_EVENT = 'COMMON_SOCKET_ERROR_EVENT';
export const COMMON_SOCKET_RECONNECTED_EVENT = 'COMMON_SOCKET_RECONNECTED_EVENT';

export function startBFFCommonSocket(hostUrl: string): void {
    if (_commonSocket) {
        return;
    }

    _commonSocket = new MicronautsWebSocketClient(
        `${hostUrl}/ws/common`,
        { autoPing: true, autoReconnect: true }
    );
    _commonSocket.onopen = (event: Event) => {
        const _event = new CustomEvent(COMMON_SOCKET_OPEN_EVENT);
        window.dispatchEvent(_event);
    };
    _commonSocket.onmessage = (event: MessageEvent) => {
        const { messageBody, topic, statusCode, headers, id } = JSON.parse(event.data);

        console.log(messageBody);

        const _parsedBody = messageBody ? JSON.parse(messageBody) : undefined;


        const _payload: WebSocketResponseMessage = {
            ...(statusCode >= 200 && statusCode < 300
                ? { data: _parsedBody }
                : { error: _parsedBody }),
            statusCode,
            headers,
            id,
            topic,
        };

        const _topicEvent = new CustomEvent(topic, { detail: _payload });
        window.dispatchEvent(_topicEvent);

        const _commonSocketEvent = new CustomEvent(COMMON_SOCKET_MESSAGE_EVENT);
        window.dispatchEvent(_commonSocketEvent);
    };
    _commonSocket.onclose = (event: CloseEvent) => {
        const _event = new CustomEvent(COMMON_SOCKET_CLOSE_EVENT);
        window.dispatchEvent(_event);
    };
    _commonSocket.onerror = (event: Event) => {
        const _event = new CustomEvent(COMMON_SOCKET_ERROR_EVENT);
        window.dispatchEvent(_event);
    };
    _commonSocket.onreconnected = (event: Event) => {
        const _event = new CustomEvent(COMMON_SOCKET_RECONNECTED_EVENT);
        window.dispatchEvent(_event);
    };
}

export function stopBFFCommonSocket(): void {
    if (_commonSocket) {
        _commonSocket.close();
        _commonSocket = undefined;
    }
}

export function sendMessage<T = any>(params: {
    subject: string;
    requestTopic: string;
    responseTopic: string;
    body?: any;
}): Promise<T> {
    const { requestTopic, responseTopic, subject, body } = params;
    const _socket = _getCommonSocket();

    return new Promise<T>((resolve, reject) => {
        const _uuid = _getUUID();
        const responseTopicWithId = `${responseTopic}_${_uuid}`;

        const responseTimeout = setTimeout(() => {
            window.removeEventListener(responseTopicWithId, _listener);
            reject(
                new Error(`Request for event "${responseTopicWithId}" timeout`)
            );
        }, _REQUEST_TIME_OUT);

        const _listener = (event: CustomEvent) => {
            clearTimeout(responseTimeout);
            window.removeEventListener(responseTopicWithId, _listener);

            const response = event.detail as WebSocketResponseMessage;
            const { data, error } = response;
            if (error) {
                reject(error);
            } else {
                resolve(data);
            }
        };

        window.addEventListener(responseTopicWithId, _listener);

        _socket.send(JSON.stringify({
            subject,
            topic: requestTopic,
            responseTopic: responseTopicWithId,
            id: _uuid,
            messageBody: body ? JSON.stringify(body) : undefined,
        }));
    });
}

export function handleEventMessage<T>(
    response: WebSocketResponseMessage,
    eventMessageHandler: (data?: T) => void,
): void {
    const { data, error } = response;
    if (!error && eventMessageHandler) {
        eventMessageHandler(data);
    }
}

// ------------------------------------------------------------ //

function _getCommonSocket(): MicronautsWebSocketClient {
    if (!_commonSocket) {
        throw new Error(`Common socket hasn't been started yet.`)
    }

    return _commonSocket;
};

function _getUUID(): string {
    let date = new Date().getTime();
    let uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, char => {
        let random = (date + Math.random() * 16) % 16 | 0;
        date = Math.floor(date / 16);
        return (char == 'x' ? random : (random & 0x3) | 0x8).toString(16);
    });
    return uuid;
}
