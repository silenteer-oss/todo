package main

import (
	"github.com/spf13/viper"
	"gitlab.com/silenteer-oss/hestia/socket_service"
)

func init() {
	viper.SetDefault("request.uri.4.validate", "/api/service/auth/validate")
	viper.SetDefault("jwt.cookie.name", "JWT")

}

func main() {
	server := socket_service.InitServer()
	server.Start()
}
