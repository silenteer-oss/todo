package main

import (
	"gitlab.com/silenteer-oss/hestia/gateway_server"
)

func main() {
	gatewayServer := gateway_server.InitGatewayServer()
	defer func() {
		gatewayServer.Stop()
	}()
	gatewayServer.Start()
}
