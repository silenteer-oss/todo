package jwt

import (
	"crypto/rsa"
	"io/ioutil"

	"github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
)

type Service struct {
	verifyKey *rsa.PublicKey
	signKey   *rsa.PrivateKey
}

func CreateJwtWithRsaFile(keyPath string) (*Service, error) {
	signBytes, err := ioutil.ReadFile(keyPath)
	if err != nil {
		return nil, errors.WithMessage(err, "Reading RSA key file error")
	}
	signKey, err := jwt.ParseRSAPrivateKeyFromPEM(signBytes)

	return CreateJwt(signKey, &signKey.PublicKey), nil
}

func CreateJwt(signKey *rsa.PrivateKey, verifyKey *rsa.PublicKey) *Service {
	return &Service{
		verifyKey: verifyKey,
		signKey:   signKey,
	}
}

func (t *Service) GenerateToken(claims jwt.Claims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodRS512, claims)
	tokenString, err := token.SignedString(t.signKey)

	if err != nil {
		return "", errors.WithMessage(err, "Token generation error")
	}
	return tokenString, nil
}

func (t *Service) ParseToken(tokenString string, claims jwt.Claims) (*jwt.Token, error) {
	// Parse the JWT string and store the result in `claims`.
	// Note that we are passing the key in this method as well. This method will return an error
	// if the token is invalid (if it has expired according to the expiry time we set on sign in),
	// or if the signature does not match
	tkn, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return t.verifyKey, nil
	})

	return tkn, err
}
