package jwt

import (
	"path"
	"path/filepath"
	"runtime"
	"testing"
	"time"

	"github.com/magiconair/properties/assert"

	"gitlab.com/silenteer-oss/titan"

	"github.com/stretchr/testify/require"

	"github.com/dgrijalva/jwt-go"
	jwtgo "github.com/dgrijalva/jwt-go"
)

type Claims struct {
	Username        string       `json:"username"`
	UserId          titan.UUID   `json:"userId"`
	ExternalUserId  titan.UUID   `json:"externalUserId"`
	CareProviderId  titan.UUID   `json:"careProviderId"`
	CareProviderKey string       `json:"careProviderKey"`
	DeviceId        string       `json:"deviceId"`
	Roles           []titan.Role `json:"roles"`
	jwtgo.StandardClaims
}

func TestJwt(t *testing.T) {
	_, filename, _, _ := runtime.Caller(0)
	projectDir := path.Join(path.Dir(filename), "../../")

	keyPath := filepath.Join(projectDir, "resources", "rsa-key-pair.pem")

	jwtService, err := CreateJwtWithRsaFile(keyPath)

	// Declare the expiration time of the token
	// here, we have kept it as 5 minutes
	expirationTime := time.Now().Add(5 * time.Minute)

	// Create the JWT claims, which includes the username and expiry time
	claims := &Claims{
		Username: "hung nguyen",
		StandardClaims: jwt.StandardClaims{
			// In JWT, the expiry time is expressed as unix milliseconds
			ExpiresAt: expirationTime.Unix(),
			NotBefore: time.Now().Unix(),
		},
	}

	// Create the JWT string
	tokenString, err := jwtService.GenerateToken(claims)
	require.NoErrorf(t, err, "JwtService token signing error: %v", err)

	// Initialize a new instance of `Claims`
	parsedClaims := &Claims{}

	// Parse the JWT string and store the result in `claims`.
	// Note that we are passing the key in this method as well. This method will return an error
	// if the token is invalid (if it has expired according to the expiry time we set on sign in),
	// or if the signature does not match
	tkn, err := jwtService.ParseToken(tokenString, parsedClaims)

	require.NoErrorf(t, err, "JwtService token parsing error: %v", err)

	require.True(t, tkn.Valid, "Token is invalid")

	// Finally, return the welcome message to the user, along with their
	// username given in the token
	assert.Equal(t, parsedClaims.Username, claims.Username)

}
