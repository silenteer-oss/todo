package app

import (
	"fmt"
	"gitlab.com/silenteer-oss/todo/ares/app/auth/api"
	"gitlab.com/silenteer-oss/todo/ares/app/auth/internal/jwt"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/viper"

	"gitlab.com/silenteer-oss/titan"
)

const (
	AuthJwtRsaKeyPath = "Auth.Jwt.RsaKeyPath"
)

func CreateHandlers() *AuthServiceImpl {
	abs := titan.AbsPathify(".")
	todoPath := "/todo/ares"
	if strings.Contains(abs, todoPath) {
		abs = abs[:strings.LastIndex(abs, todoPath)+len(todoPath)]
		viper.SetDefault(AuthJwtRsaKeyPath, filepath.Join(abs, "app", "auth", "resources", "rsa-key-pair.pem"))
	} else {
		viper.SetDefault(AuthJwtRsaKeyPath, filepath.Join(abs, "resources", "rsa-key-pair.pem"))
	}

	logger := titan.GetLogger()

	jwtService, err := jwt.CreateJwtWithRsaFile(viper.GetString(AuthJwtRsaKeyPath))
	if err != nil {
		logger.Error(fmt.Sprintf("RSA key parsing error: %+v\n ", err))
		os.Exit(1)
	}

	return &AuthServiceImpl{
		jwtService: jwtService,
	}

}

func NewServer() *titan.Server {
	authService := CreateHandlers()
	return api.NewServer(authService)
}
