package app

import (
	"encoding/gob"
	"gitlab.com/silenteer-oss/hestia/infrastructure"
	"gitlab.com/silenteer-oss/todo/ares/app/auth/api"
	"gitlab.com/silenteer-oss/todo/ares/app/auth/internal/jwt"
	"math"
	"net/http"
	"time"

	jwtgo "github.com/dgrijalva/jwt-go"

	_ "github.com/google/uuid"
	"gitlab.com/silenteer-oss/titan"
)

const JWT_KEY = "JWT"

func init() {
	gob.Register(&Claims{})
	gob.Register([]titan.Role{})
	gob.Register([]titan.UUID{})
}

type Claims struct {
	CareProviderId titan.UUID   `json:"careProviderId"`
	UserId         titan.UUID   `json:"userId"`
	Roles          []titan.Role `json:"roles"`
	jwtgo.StandardClaims
}

func (c *Claims) GetFirstRole() *titan.Role {
	if c.Roles == nil || len(c.Roles) == 0 {
		return nil
	}
	return &c.Roles[0]
}

type AuthServiceImpl struct {
	jwtService *jwt.Service
}

func (a *AuthServiceImpl) Login(ctx *titan.Context, credentials *api.UserIdPasswordCredentials) (*titan.Response, error) {
	if credentials.UserId == "" {
		return titan.NewResBuilder().StatusCode(401).Build(), nil
	}

	builder := titan.NewResBuilder()
	claims := &Claims{}

	token := getTokenFromRequest(ctx)
	if token != "" {
		jwtToken, err := a.jwtService.ParseToken(token, claims)
		if err != nil {
			if err == jwtgo.ErrSignatureInvalid {
				return builder.StatusCode(http.StatusUnauthorized).Build(), nil
			}
			return builder.StatusCode(http.StatusBadRequest).Build(), nil
		}
		if !jwtToken.Valid {
			return builder.StatusCode(http.StatusBadRequest).Build(), nil
		}
	} else {
		claims.CareProviderId = titan.UUID(credentials.CareProviderId)
		claims.UserId = titan.UUID(credentials.UserId)
		claims.Roles = []titan.Role{infrastructure.CARE_PROVIDER_MEMBER}
	}

	return a.LoginSuccess(ctx, claims)
}

func (a *AuthServiceImpl) LoginSuccess(ctx *titan.Context, userDetails *Claims) (*titan.Response, error) {
	userDetailInToken := &Claims{
		CareProviderId: userDetails.CareProviderId,
		UserId:         userDetails.UserId,
		Roles:          userDetails.Roles,
		StandardClaims: jwtgo.StandardClaims{
			IssuedAt:  time.Now().Unix(),
			Issuer:    "authentication-service",
			NotBefore: time.Now().Unix(),
			Subject:   string(userDetails.UserId),
		},
	}

	accessToken, err := a.jwtService.GenerateToken(userDetailInToken)
	if err != nil {
		return nil, err
	}

	cookie := &http.Cookie{
		Name:     JWT_KEY,
		Value:    accessToken,
		Path:     "/",
		Domain:   "",
		MaxAge:   math.MaxInt32,
		Secure:   false,
		HttpOnly: false,
		Expires:  time.Now().Add(time.Duration(math.MaxInt32) * time.Second),
	}

	response := titan.
		NewResBuilder().
		StatusCode(200).
		BodyJSON(map[string]string{"access_token": accessToken}).Build()

	http.SetCookie(response, cookie)

	return response, nil
}

func (a *AuthServiceImpl) Validate(ctx *titan.Context) (*titan.UserInfo, error) {
	token := getTokenFromRequest(ctx)

	if token == "" {
		return nil, nil
	}

	claims := &Claims{}

	jwtToken, err := a.jwtService.ParseToken(token, claims)
	if err != nil {
		if err == jwtgo.ErrSignatureInvalid {
			return nil, &titan.CommonException{Status: http.StatusUnauthorized}
		}
		return nil, &titan.CommonException{Status: http.StatusBadRequest}
	}
	if !jwtToken.Valid {
		return nil, &titan.CommonException{Status: http.StatusBadRequest}
	}

	userInfo := &titan.UserInfo{
		CareProviderId: claims.CareProviderId,
		UserId:         claims.UserId,
		Role:           *claims.GetFirstRole(),
		Attributes: map[string]interface{}{
			"roles": claims.Roles,
			"sub":   claims.Subject,
			"nbf":   claims.NotBefore,
			"iss":   claims.Issuer,
			"iat":   claims.IssuedAt,
		},
	}

	return userInfo, nil
}

func getTokenFromRequest(ctx *titan.Context) string {
	logger := ctx.Logger()
	r := ctx.Request()

	c, err := r.Cookie(JWT_KEY)
	if err != nil {
		logger.Debug("jwt not found")
		return ""
	}
	return c.Value
}
