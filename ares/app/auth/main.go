package main

import (
	app2 "gitlab.com/silenteer-oss/todo/ares/app/auth/internal/app"
)

func main() {
	app2.NewServer().Start()
}
