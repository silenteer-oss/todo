package test

import (
	"gitlab.com/silenteer-oss/hestia/infrastructure"
	"gitlab.com/silenteer-oss/todo/ares/app/auth/api"
	"gitlab.com/silenteer-oss/todo/ares/app/auth/internal/app"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"testing"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"gitlab.com/silenteer-oss/titan"
	"gitlab.com/silenteer-oss/titan/test"
)

var (
	natsClient  *titan.Client
	authService *api.AuthServiceClient
)

func init() {
	natsClient = titan.GetDefaultClient()
	authService = api.NewAuthServiceClient()

	_, filename, _, _ := runtime.Caller(0)
	projectDir := path.Join(path.Dir(filename), "..")

	keyPath := filepath.Join(projectDir, "resources", "rsa-key-pair.pem")
	viper.Set("Auth.Jwt.RsaKeyPath", keyPath)
}

func TestMain(m *testing.M) {
	testServer := test.NewTestServer(nil, app.NewServer())
	testServer.Start()

	exitVal := m.Run()

	testServer.Stop()
	os.Exit(exitVal)
}

func TestLogin(t *testing.T) {
	userIdPasswordCredentials := &api.UserIdPasswordCredentials{
		UserId: "some-user-id",
	}

	resp, err := authService.Login(titan.NewBackgroundContext(), userIdPasswordCredentials)

	assert.Nil(t, err)
	assert.NotNil(t, resp)
}

func TestValidate(t *testing.T) {
	userIdPasswordCredentials := &api.UserIdPasswordCredentials{
		CareProviderId: "some-care-provider-id",
		UserId:         "some-user-id",
	}
	loginResp, _ := authService.Login(titan.NewBackgroundContext(), userIdPasswordCredentials)

	req, _ := titan.NewReqBuilder().
		Post(api.EVENT_Validate).
		SetHeaders(http.Header{
			"Cookie": []string{loginResp.Headers.Get("Set-Cookie")},
		}).
		Subject(api.NATS_SUBJECT).
		Build()
	var resp = &titan.UserInfo{}
	err := natsClient.SendAndReceiveJson(titan.NewBackgroundContext(), req, &resp)

	assert.Nil(t, err)
	assert.Equal(t, titan.UUID("some-care-provider-id"), resp.CareProviderId)
	assert.Equal(t, titan.UUID("some-user-id"), resp.UserId)
	assert.Equal(t, infrastructure.CARE_PROVIDER_MEMBER, resp.Role)
}
