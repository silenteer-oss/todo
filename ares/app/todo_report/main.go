package main

import (
	socket_api "gitlab.com/silenteer-oss/hestia/socket_service/api"
	"gitlab.com/silenteer-oss/todo/ares/app/todo_report/api/todo_report_bff"
	"gitlab.com/silenteer-oss/todo/ares/app/todo_report/internal"
	"gitlab.com/silenteer-oss/todo/ares/service/domains/repos/app/todo"
)

func main() {
	db := todo.NewTodoDefaultRepository()
	socket := socket_api.NewSocketServiceClient()

	todoReportBff := internal.NewTodoReportBff(&db, socket)

	todo_report_bff.NewServer(todoReportBff).Start()
}
