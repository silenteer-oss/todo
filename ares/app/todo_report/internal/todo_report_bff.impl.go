package internal

import (
	socket_api "gitlab.com/silenteer-oss/hestia/socket_service/api"
	"gitlab.com/silenteer-oss/titan"
	"gitlab.com/silenteer-oss/todo/ares/app/todo_report/api/todo_report_bff"
	todo_service_api "gitlab.com/silenteer-oss/todo/ares/service/domains/api/todo_service"
	"gitlab.com/silenteer-oss/todo/ares/service/domains/repos/app/todo"
	"go.mongodb.org/mongo-driver/bson"
)

type TodoReportBff struct {
	db       *todo.TodoDefaultRepository
	notifier *todo_report_bff.TodoReportBffSocketNotifier
}

func NewTodoReportBff(db *todo.TodoDefaultRepository, socket *socket_api.SocketServiceClient) *TodoReportBff {
	notifier := todo_report_bff.NewTodoReportBffSocketNotifier(socket)
	return &TodoReportBff{db: db, notifier: notifier}
}

func (b *TodoReportBff) LoadTodoReport(ctx *titan.Context) (*todo_report_bff.TodoReportResponse, error) {
	filter := bson.D{{}}

	count, err := b.db.Count(ctx, filter)

	if err != nil {
		return nil, err
	}

	return &todo_report_bff.TodoReportResponse{Total: count}, nil
}

func (b *TodoReportBff) HandleDomainTodoAdded(ctx *titan.Context, response *todo_service_api.EventTodoAdded) error {
	filter := bson.D{{}}

	count, err := b.db.Count(ctx, filter)

	if err == nil {
		event := todo_report_bff.EventTodoReportUpdated{Total: count}
		_ = b.notifier.NotifyCareProviderTodoReportUpdated(ctx, &event)
		return nil
	} else {
		return err
	}
}

func (b *TodoReportBff) HandleDomainTodoRemoved(ctx *titan.Context, response *todo_service_api.EventTodoRemoved) error {
	filter := bson.D{{}}

	count, err := b.db.Count(ctx, filter)

	if err == nil {
		event := &todo_report_bff.EventTodoReportUpdated{Total: count}
		_ = b.notifier.NotifyCareProviderTodoReportUpdated(ctx, event)
		return nil
	} else {
		return err
	}
}
