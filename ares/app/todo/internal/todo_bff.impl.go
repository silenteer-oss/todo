package internal

import (
	socket_api "gitlab.com/silenteer-oss/hestia/socket_service/api"
	"gitlab.com/silenteer-oss/titan"
	"gitlab.com/silenteer-oss/todo/ares/app/todo/api/todo_bff"
	todo_domain_api "gitlab.com/silenteer-oss/todo/ares/service/domains/api/todo_service"
	todo_repo "gitlab.com/silenteer-oss/todo/ares/service/domains/repos/app/todo"
	"go.mongodb.org/mongo-driver/bson"
)

type TodoBffImpl struct {
	db       *todo_repo.TodoDefaultRepository
	client   *todo_domain_api.TodoServiceClient
	notifier *todo_bff.TodoBffSocketNotifier
}

func NewTodoBff(db *todo_repo.TodoDefaultRepository, socket *socket_api.SocketServiceClient) *TodoBffImpl {
	client := todo_domain_api.NewTodoServiceClient()
	notifier := todo_bff.NewTodoBffSocketNotifier(socket)
	return &TodoBffImpl{db: db, client: client, notifier: notifier}
}

func (b *TodoBffImpl) AddTodo(ctx *titan.Context, request *todo_domain_api.TodoRequest) (*todo_bff.EventTodoAdded, error) {
	res, err := b.client.AddTodo(ctx, request)
	if err != nil {
		return nil, err
	}

	event := &todo_bff.EventTodoAdded{
		Id:       res.Id,
		Todo:     res.Todo,
		Priority: res.Priority,
	}

	return event, nil

}

func (b *TodoBffImpl) RemoveTodo(ctx *titan.Context, request *todo_domain_api.TodoId) (*todo_bff.EventTodoRemoved, error) {
	res, err := b.client.RemoveTodo(ctx, request)
	if err != nil {
		return nil, err
	}
	event := &todo_bff.EventTodoRemoved{
		Id: res.Id,
	}
	return event, nil
}

func (b *TodoBffImpl) LoadTodo(ctx *titan.Context) (*todo_bff.TodoResponses, error) {
	todos, err := b.db.Find(ctx, bson.D{{}})

	if err != nil {
		return nil, err
	}

	var todoResponses []*todo_domain_api.TodoResponse
	for _, todo := range todos {
		todoResponses = append(todoResponses, &todo_domain_api.TodoResponse{
			Id:       todo.Id,
			Todo:     todo.Todo,
			Priority: todo_domain_api.Priority(todo.Priority),
		})
	}

	return &todo_bff.TodoResponses{Responses: todoResponses}, nil
}

func (b *TodoBffImpl) HandleDomainTodoAdded(ctx *titan.Context, response *todo_domain_api.EventTodoAdded) error {
	event := &todo_bff.EventTodoAdded{
		Id:       response.Id,
		Todo:     response.Todo,
		Priority: response.Priority,
	}
	_ = b.notifier.NotifyCareProviderTodoAdded(ctx, event)
	return nil

}

func (b *TodoBffImpl) HandleDomainTodoRemoved(ctx *titan.Context, response *todo_domain_api.EventTodoRemoved) error {
	event := &todo_bff.EventTodoRemoved{Id: response.Id}
	_ = b.notifier.NotifyCareProviderTodoRemoved(ctx, event)
	return nil
}
