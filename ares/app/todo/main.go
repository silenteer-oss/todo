package main

import (
	socket_api "gitlab.com/silenteer-oss/hestia/socket_service/api"
	"gitlab.com/silenteer-oss/todo/ares/app/todo/api/todo_bff"
	"gitlab.com/silenteer-oss/todo/ares/app/todo/internal"
	"gitlab.com/silenteer-oss/todo/ares/service/domains/repos/app/todo"
)

func main() {
	db := todo.NewTodoDefaultRepository()
	socket := socket_api.NewSocketServiceClient()

	todoBff := internal.NewTodoBff(&db, socket)

	todo_bff.NewServer(todoBff).Start()
}
