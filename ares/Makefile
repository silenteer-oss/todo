SHELL 	 := /bin/bash
GITLAB := gitlab.com
GITLAB_REPO := $(GITLAB)/silenteer-oss/todo
GITLAB_REPO_ESCAPED := $(subst /,\/,$(GITLAB_REPO))
GITLAB_REGISTRY := registry.$(GITLAB_REPO)
GOFF_REPO := $(GITLAB)/silenteer-oss/goff
GOFF_EXE := protoc-gen-goff

TARGETS := $(sort $(patsubst ./%/main.go,%,$(wildcard ./*/*/main.go/)))
TARGETS_CLEAN := $(patsubst %,clean-%, $(TARGETS))
TARGETS_BUILD := $(patsubst %,build-%, $(TARGETS))
TARGETS_TEST := $(patsubst %,test-%, $(TARGETS))
TARGETS_DOCKER := $(patsubst %,docker-%, $(TARGETS))

.PHONY: clean build test docker codegen ${TARGETS_BUILD} ${TARGETS_BUILD} $(TARGETS_TEST) $(TARGETS_DOCKER)

codegen:
	@command -v protoc >/dev/null 2>&1 || { echo >&2 "Error: protoc is not installed.  Aborting."; exit 1; }
# 	@command -v tsfmt >/dev/null 2>&1 || { echo >&2 "Error: tsfmt is not installed.  Aborting."; exit 1; }
# 	@go get $(GOFF_REPO)
	@go install $(GOFF_REPO)/$(GOFF_EXE)
	@command -v $(GOFF_EXE)>/dev/null 2>&1 || { echo >&2 "Error: $(GOFF_EXE) is not found. Please ensure that GOPATH/bin is in your PATH variable."; exit 1; }
	@go install gitlab.com/silenteer-oss/goff/protoc-gen-dbconfig
	@command -v protoc-gen-dbconfig>/dev/null 2>&1 || { echo >&2 "Error: protoc-gen-dbconfig is not found. Please ensure that $GOPATH/bin is in your PATH variable."; exit 1; }
	@set -e;\
	mkdir -p ../hermes/src/bff; \
	for SOURCE in $$(find ./proto -name "*.proto"); do \
		if [[ "$$file" != "" ]] && [[  "$$file" != "$$(basename $$SOURCE)" ]]; then \
			continue; \
		fi; \
		echo Generating code from $$SOURCE; \
		PROTO_PACKAGE_AS_TS_FILE=true DB_PREFIX="" DB_SUFFIX=_db  protoc  -I `go list -m -f "{{.Dir}}" $(GOFF_REPO)` -I ./proto   --goff_out ./  $$SOURCE; \
		PACKAGE=$$(sed -n -e '/^option go_package/p' $$SOURCE | cut -d = -f 2 | cut -d \" -f 2); \
		TARGET_DIR=$$(echo $$PACKAGE | sed -e "s/^$(GITLAB_REPO_ESCAPED)\/ares/./"); \
		mkdir -p $$TARGET_DIR; \
		cp -rf ./$$PACKAGE/*.go $$TARGET_DIR/; \
		if [ -e ./$$PACKAGE/*.ts ]; then \
			tsfmt -r ./$$PACKAGE/*.ts; \
			tsfmt -r ./$$PACKAGE/legacy/*.ts; \
			cp -rf ./$$PACKAGE/*.ts ../hermes/src/bff/; \
			cp -rf ./$$PACKAGE/legacy/*.ts ../hermes/src/bff/legacy/; \
		fi; \
	done; \
	rm -rf ./$(GITLAB);
	DB_PREFIX="" DB_SUFFIX=_db protoc-gen-dbconfig -co=$$(pwd)/service/db_gateway/client/dbconfig.gen.go -cp=client -fo=$$(pwd)/pkg/test/data/repository.gen.go -fp=data $$(pwd)/proto/repo

clean: $(TARGETS_CLEAN)

$(TARGETS_CLEAN): clean-%: %
	@echo Cleaning [$*]
	rm -rf ./$*/bin

build: $(TARGETS_BUILD)

$(TARGETS_BUILD): build-%: clean-%
	@echo Building [$*]
	go build -o ./$*/bin/$* ./$*/main.go

test:
	go test -p 1 ./...

$(TARGETS_TEST): test-%: %
	go test -p 1 -v ./$*/...

docker: $(TARGETS_DOCKER)

$(TARGETS_DOCKER): docker-%: clean-%
	@echo Building [$*]
	app=$$(basename "$*"); \
    tag=$$(echo "$*" | sed -e 's/\//_/g');\
	GOOS=linux GO111MODULE=on CGO_ENABLED=0 go build -ldflags '-w' -o ./$*/bin/$$app ./$*/main.go ; \
	upx ./$*/bin/$$app; \
	docker build --build-arg TARGET=$$(basename "$*") -t $(GITLAB_REGISTRY)/$$tag:$$VERSION -f Dockerfile ./$* ;  \
	docker push $(GITLAB_REGISTRY)/$$tag:$$VERSION; \

run:
	@set -e ;\
	CURRENT_DIR="$$(pwd)"; \
	for app in $(TARGETS);						 \
	do                                       \
		echo $$app;\
		if [[ $$EXCLUDE =~ (^|,)$$app(,|$$) ]]; then             \
			echo "Skipping application $$app";	\
			continue;                           \
		fi;                                 \
		echo "Starting application $$app" ; \
		cd "$${CURRENT_DIR}/$$app";         \
		go run main.go & pid=$$!   ;       \
		PID_LIST+=" $$pid" ;				\
		pstree $$pid ; 						\
	done ; 									\
	trap "kill $$PID_LIST" SIGINT ;           \
	echo "All applications have started $$PID_LIST" ; \
	wait $${PID_LIST}  ;  					 \
	echo "All applications have completed";  \

help:
	@echo "Targets:"
	@echo "     $(TARGETS)"
	@echo "Usages:"
	@echo "     make run                                    run all targets at the same time. Useful for development environment"
	@echo "     make run EXCLUDE=service/domains,app/todo   run all targets at the same time except target(s). Useful for debugging that exclusion running from IDE"
	@echo "     make clean                                  clean up all the built executables"
	@echo "     make build                                  build all targets"
	@echo "     make build-app/auth                         build specific target"
	@echo "     make test                                   test all targets"
	@echo "     make test-app/auth                          test specific target"
	@echo "     make docker VERSION=v1.0.1                  build docker image for all targets"
	@echo "     make docker-app/auth VERSION=v1.0.1         build docker image for specific target"
	@echo "     make codegen                                generate codes from proto files in folder ./proto"
	@echo "     make codegen file=auth_app.proto            generate codes from auth_app.proto in ./proto or it recursive sub folders"
