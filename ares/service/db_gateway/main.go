package main

import (
	"gitlab.com/silenteer-oss/hestia/db_gateway_service"
	"gitlab.com/silenteer-oss/todo/ares/service/db_gateway/client"
)

func main() {

	server, cancel := db_gateway_service.InitDatabaseGateway(client.GetDbConfig())

	defer func() {
		cancel()
		server.Stop()
	}()

	server.Start()

}
