package app

import (
	"gitlab.com/silenteer-oss/titan"
	"gitlab.com/silenteer-oss/todo/ares/service/domains/api/todo_service"
	"gitlab.com/silenteer-oss/todo/ares/service/domains/internal/app/todo"
)

func NewServer() *titan.Server {

	toDoDomain := internal.NewTodoService()
	todoRouter := todo_service.NewTodoServiceRouter(toDoDomain)

	return titan.NewServer(
		"api.service.domains",
		titan.Routes(todoRouter.Register),
	)
}
