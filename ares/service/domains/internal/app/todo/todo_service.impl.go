package internal

import (
	"errors"
	"github.com/google/uuid"
	"gitlab.com/silenteer-oss/titan"
	"gitlab.com/silenteer-oss/todo/ares/service/domains/api/todo_service"
	"gitlab.com/silenteer-oss/todo/ares/service/domains/repos/app/todo"
)

type TodoService struct {
	db       *todo.TodoDefaultRepository
	notifier *todo_service.TodoServiceNotifier
}

func NewTodoService() *TodoService {
	notifier := todo_service.NewTodoServiceNotifier()
	repository := todo.NewTodoDefaultRepository()
	return &TodoService{
		db:       &repository,
		notifier: notifier,
	}
}

func (t *TodoService) AddTodo(ctx *titan.Context, request *todo_service.TodoRequest) (*todo_service.TodoResponse, error) {
	id := uuid.New()
	todo := todo.Todo{
		Id:       &id,
		Todo:     request.Todo,
		Priority: todo.Priority(request.Priority),
	}

	resp, err := t.db.Create(ctx, todo)

	if err != nil {
		return nil, err
	}

	if resp.Id == nil {
		return nil, errors.New("no_response_data")
	}

	event := &todo_service.EventTodoAdded{
		Id:       resp.Id,
		Todo:     resp.Todo,
		Priority: todo_service.Priority(resp.Priority),
	}

	err = t.notifier.NotifyTodoAdded(ctx, event)

	return &todo_service.TodoResponse{
		Id:       resp.Id,
		Todo:     resp.Todo,
		Priority: todo_service.Priority(resp.Priority),
	}, err
}

func (t *TodoService) RemoveTodo(ctx *titan.Context, request *todo_service.TodoId) (*todo_service.TodoId, error) {
	deleted, err := t.db.DeleteById(ctx, *request.Id)

	if err != nil {
		return nil, err
	}
	if !deleted {
		return nil, errors.New("delete_failed")
	}

	event := &todo_service.EventTodoRemoved{
		Id: request.Id,
	}

	err = t.notifier.NotifyTodoRemoved(ctx, event)

	return request, err
}
