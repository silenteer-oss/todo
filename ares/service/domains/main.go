package main

import (
	"gitlab.com/silenteer-oss/todo/ares/service/domains/app"
)

func main() {
	server := app.NewServer()

	server.Start()

	defer func() {
		server.Stop()
	}()
}
