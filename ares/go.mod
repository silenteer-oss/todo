module gitlab.com/silenteer-oss/todo/ares

go 1.13

require (
	github.com/deepmap/oapi-codegen v1.3.8 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/golang/protobuf v1.4.2
	github.com/google/uuid v1.1.1
	github.com/magiconair/properties v1.8.1
	github.com/nats-io/nats-server/v2 v2.1.7 // indirect
	github.com/pkg/errors v0.9.1
	github.com/spf13/viper v1.7.0
	github.com/stretchr/testify v1.5.1
	github.com/xakep666/mongo-migrate v0.2.1 // indirect
	gitlab.com/silenteer-oss/goff v1.0.74
	gitlab.com/silenteer-oss/hestia v1.0.43
	gitlab.com/silenteer-oss/titan v1.0.35
	go.mongodb.org/mongo-driver v1.3.5
)
