import { TTodoReportI18nKeys } from './TodoReport.i18n.vi';

const translation: { [key in TTodoReportI18nKeys]: string } = {
  moduleName: 'Todo Report',
};

export default translation;
