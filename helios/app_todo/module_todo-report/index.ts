import { ComponentType } from 'react';
import TodoReport from './TodoReport.styled';
import { ITodoReportProps } from './TodoReport';

export interface ITodoReportModule {
  components: {
    TodoReport: ComponentType<ITodoReportProps>;
  };
}

const TodoReportModule = {
  components: {
    TodoReport,
  },
};

export default TodoReportModule;
