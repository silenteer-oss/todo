const translation = {
  moduleName: 'Báo cáo công việc',
};

export default translation;

export type TTodoReportI18nKeys = keyof typeof translation;
