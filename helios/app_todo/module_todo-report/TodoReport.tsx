import { LoadingState } from '@design-system/core/components';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import {
  loadTodoReport,
  useListenTodoReportUpdated,
} from '@silenteer/hermes/bff/todo_report_bff';
import { ITodoThemeProps } from '@todo/theme';
import React, { memo, useEffect, useState } from 'react';
import { TTodoReportI18nKeys } from './TodoReport.i18n.vi';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ITodoReportProps {}

function TodoReport(
  props: ITodoReportProps &
    ITodoThemeProps &
    II18nFixedNamespaceContext<TTodoReportI18nKeys>
) {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [todoTotal, setTodoTotal] = useState<number>(0);

  useListenTodoReportUpdated((response) => {
    setTodoTotal(response?.total);
  });

  useEffect(() => {
    loadTodoReport().then((result) => {
      setIsLoading(false);
      setTodoTotal(result.total);
    });
  }, []);

  return (
    <div className={props.className}>
      {isLoading && <LoadingState />}
      {!isLoading && <div className="sl-todo-total">{todoTotal}</div>}
    </div>
  );
}

export default memo(
  withI18nContext(TodoReport, {
    namespace: '@todo/module_todo-report/TodoReport',
  })
);
