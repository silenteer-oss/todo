import { ComponentType } from 'react';
import Theme, { ITodoStyledComponent } from '@todo/theme';
import OriginalTodoReport, { ITodoReportProps } from './TodoReport';

const TodoReport: ITodoStyledComponent<ComponentType<
  ITodoReportProps
>> = Theme.styled(OriginalTodoReport)`
  ${(props) => {
    const {
      theme: { foreground },
    } = props;

    return `
      backgroundColor: ${foreground.primary.base}
    `;
  }}
`;

export default TodoReport;
