import { getConcreteStyledComponent } from '@design-system/themes/models';
import {
  IDefaultTheme,
  BaseDefaultTheme,
  getTextAlignClass,
  getAllComponentTheme,
  mixGlobalStyle,
  mixCustomBlueprintStyle,
} from '@infrastructure/themes/default';
import { StyledComponentBase, ThemeProps } from 'styled-components';
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IViewsTheme {}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ITodoTheme extends IDefaultTheme<IViewsTheme> {}

class TodoTheme extends BaseDefaultTheme<IViewsTheme> {}

const instance = new TodoTheme();
instance.setComponentTheme(getAllComponentTheme);

export default {
  ...getConcreteStyledComponent<ITodoTheme>(),
  instance,
  getTextAlignClass,
  mixGlobalStyle,
  mixCustomBlueprintStyle,
};

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ITodoThemeProps extends ThemeProps<ITodoTheme> {
  className: string;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ITodoStyledComponent<
  C extends keyof JSX.IntrinsicElements | React.ComponentType<any>,
  O extends object = {},
  A extends keyof any = never
> extends StyledComponentBase<C, ITodoTheme, O, A> {}
