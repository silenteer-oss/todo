const translation = {
  login: 'Đăng nhập',
};

export default translation;

export type TLoginI18nKeys = keyof typeof translation;
