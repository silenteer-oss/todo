import { Button } from '@blueprintjs/core';
import { Flex } from '@design-system/core/components';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { getUUID } from '@design-system/infrastructure/utils';
import { getImageUrl } from '@infrastructure/static-url';
import { login } from '@silenteer/hermes/bff/legacy/auth_app';
import { ITodoThemeProps } from '@todo/theme';
import React, { memo } from 'react';
import { TLoginI18nKeys } from './Login.i18n.vi';

export interface ILoginProps {
  onLoginSuccess: () => void;
}

const LogoSVG = getImageUrl('logo.svg');

function Login(
  props: ILoginProps &
    ITodoThemeProps &
    II18nFixedNamespaceContext<TLoginI18nKeys>
) {
  const { t } = props;

  const handleLogin = async () => {
    await login({
      careProviderId: getUUID(),
      userId: getUUID(),
      jwtToken: '',
    });

    props.onLoginSuccess();
  };

  return (
    <div className={props.className}>
      <Flex justify="center" align="center" column>
        <img src={LogoSVG} />
        <br />
        <br />
        <Button text={t('login')} intent="primary" onClick={handleLogin} />
        <br />
        <br />
      </Flex>
    </div>
  );
}

export default memo(
  withI18nContext(Login, {
    namespace: '@todo/module_login/Login',
  })
);
