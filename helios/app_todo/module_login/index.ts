import { ComponentType } from 'react';
import Login from './Login.styled';
import { ILoginProps } from './Login';

export interface ILoginModule {
  components: {
    Login: ComponentType<ILoginProps>;
  };
}

const LoginModule = {
  components: {
    Login,
  },
};

export default LoginModule;
