import { ComponentType } from 'react';
import Theme, { ITodoStyledComponent } from '@todo/theme';
import OriginalLogin, { ILoginProps } from './Login';

const Login: ITodoStyledComponent<ComponentType<ILoginProps>> = Theme.styled(
  OriginalLogin
)`
  ${(props) => {
    const {
      theme: { space, background },
    } = props;

    return `
      width: 100%;
      height: 100%;
      padding: ${space.m};

      .sl-todo-list {
        max-width: 50%;
        margin: 0 auto;

        .sl-todo-item {
          padding: ${space.s} 0;

          + .sl-todo-item {
            border-top: 1px solid ${background['02']};
          }
        }
      }
    `;
  }}
`;

export default Login;
