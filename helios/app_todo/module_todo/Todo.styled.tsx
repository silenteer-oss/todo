import { ComponentType } from 'react';
import Theme, { ITodoStyledComponent } from '@todo/theme';
import OriginalTodo, { ITodoProps } from './Todo';

const Todo: ITodoStyledComponent<ComponentType<ITodoProps>> = Theme.styled(
  OriginalTodo
)`
  ${(props) => {
    const {
      theme: { space, background },
    } = props;

    return `
      width: 100%;
      height: 100%;
      padding: ${space.m};

      .sl-todo-list {
        margin: 0 auto;

        .sl-todo-item {
          padding: ${space.s} 0;

          + .sl-todo-item {
            border-top: 1px solid ${background['02']};
          }

          .sl-todo-item-todo {
            width: 70%;
            padding-left: 16px;
            padding-right: 16px;
          }
          .sl-todo-item-priority {
            width: 20%;
            padding-left: 16px;
          }
          .sl-todo-item-button {
            width: 10%;
          }
        }
      }

      .sl-todo-input {
        .sl-todo-input-todo {
          width: 70%;
          padding-right: 16px;
        }
        .sl-todo-input-priority {
          width: 20%;
        }
        .sl-todo-input-button {
          width: 10%;
        }
      }
    `;
  }}
`;

export default Todo;
