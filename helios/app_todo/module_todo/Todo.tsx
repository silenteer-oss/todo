import { Button, InputGroup, MenuItem } from '@blueprintjs/core';
import { Select } from '@blueprintjs/select';
import { Box, Flex, LoadingState } from '@design-system/core/components';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import {
  addTodo,
  loadTodo,
  removeTodo,
  useListenTodoAdded,
  useListenTodoRemoved,
} from '@silenteer/hermes/bff/todo_bff';
import { Priority, TodoResponse } from '@silenteer/hermes/bff/todo_service';
import { ITodoThemeProps } from '@todo/theme';
import React, { memo, useEffect, useState } from 'react';
import { TTodoI18nKeys } from './Todo.i18n.vi';
import {
  addResponseToTodoList,
  removeResponseFromTodoList,
} from './Todo.service';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ITodoProps {}

const PrioritySelect = Select.ofType<Priority>();

function Todo(
  props: ITodoProps &
    ITodoThemeProps &
    II18nFixedNamespaceContext<TTodoI18nKeys>
) {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [currentTodo, setCurrentTodo] = useState<string>();
  const [currentPriority, setCurrentPriority] = useState<Priority>();
  const [isAdding, setIsAdding] = useState<boolean>(false);
  const [todoList, setTodoList] = useState<TodoResponse[]>([]);
  const { t } = props;

  useListenTodoAdded((response) => {
    setTodoList(prevTodoList => addResponseToTodoList(response, prevTodoList));
  });
  useListenTodoRemoved((response) => {
    setTodoList(prevTodoList => removeResponseFromTodoList(response, prevTodoList));
  });

  useEffect(() => {
    loadTodo().then((result) => {
      setIsLoading(false);
      setTodoList(result.responses || []);
    });
  }, []);

  const selectPriorityItemRender = (item, { handleClick }) => {
    return (
      <MenuItem
        key={item}
        text={item}
        onClick={handleClick}
        shouldDismissPopover={true}
      />
    );
  };

  return (
    <div className={props.className}>
      <Flex className="sl-todo-input" align="center">
        <Box className="sl-todo-input-todo">
          <InputGroup
            type="text"
            placeholder={t('inputTodoDetails')}
            value={currentTodo}
            onChange={(e) => setCurrentTodo(e.target.value)}
            disabled={isAdding}
          />
        </Box>
        <PrioritySelect
          className="sl-todo-input-priority"
          items={[Priority.High, Priority.Medium, Priority.Low]}
          itemRenderer={selectPriorityItemRender}
          onItemSelect={setCurrentPriority}
          filterable={false}
          popoverProps={{ usePortal: false }}
        >
          <Button
            text={currentPriority || t('selectPriority')}
            rightIcon="caret-down"
          />
        </PrioritySelect>
        <Button
          className="sl-todo-input-button"
          text={t('add')}
          intent="primary"
          loading={isAdding}
          onClick={() => {
            setIsAdding(true);
            addTodo({ todo: currentTodo, priority: currentPriority })
              .then((response) => {
                addResponseToTodoList(response, todoList);
                setCurrentTodo('');
                setCurrentPriority(null);
              })
              .finally(() => {
                setIsAdding(false);
              });
          }}
          onSubmit={(e) => {
            e.stopPropagation();
            e.preventDefault();
          }}
          disabled={!currentTodo || !currentPriority}
        />
      </Flex>

      {isLoading && <LoadingState />}
      {!isLoading && (
        <div className="sl-todo-list">
          {todoList.map((todo) => (
            <Flex className="sl-todo-item" key={todo.id} align="center">
              <Box className="sl-todo-item-todo">{todo.todo}</Box>
              <Box className="sl-todo-item-priority">{todo.priority}</Box>
              <Button
                className="sl-todo-item-button"
                text={t('remove')}
                intent="danger"
                loading={isAdding}
                onClick={() => {
                  removeTodo({ id: todo.id }).then((response) => {
                    setTodoList((todoList) =>
                      removeResponseFromTodoList(response, todoList)
                    );
                  });
                }}
                onSubmit={(e) => {
                  e.stopPropagation();
                  e.preventDefault();
                }}
              />
            </Flex>
          ))}
        </div>
      )}
    </div>
  );
}

export default memo(
  withI18nContext(Todo, {
    namespace: '@todo/module_todo/Todo',
  })
);
