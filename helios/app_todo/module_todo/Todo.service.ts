import {
  TodoResponse,
  EventTodoAdded,
  EventTodoRemoved,
} from '@silenteer/hermes/bff/todo_service';

export function addResponseToTodoList(
  response: EventTodoAdded,
  todoList: TodoResponse[]
) {
  if (!response) {
    return todoList;
  }

  const index = todoList.findIndex((todo) => todo.id === response.id);
  if (index >= 0) {
    return todoList;
  }
  return [response, ...todoList];
}

export function removeResponseFromTodoList(
  response: EventTodoRemoved,
  todoList: TodoResponse[]
) {
  if (!response) {
    return todoList;
  }

  const index = todoList.findIndex((todo) => todo.id === response.id);
  if (index < 0) {
    return todoList;
  }

  const result = [...todoList];
  if (index >= 0) {
    result.splice(index, 1);
  }

  return result;
}
