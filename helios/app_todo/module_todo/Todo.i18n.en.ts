import { TTodoI18nKeys } from './Todo.i18n.vi';

const translation: { [key in TTodoI18nKeys]: string } = {
  moduleName: 'Todo Management',
  inputTodoDetails: 'Input todo details',
  selectPriority: 'Select priority',
  add: 'Add',
  remove: 'Remove',
};

export default translation;
