import { ComponentType } from 'react';
import Todo from './Todo.styled';
import { ITodoProps } from './Todo';

export interface ITodoModule {
  components: {
    Todo: ComponentType<ITodoProps>;
  };
}

const TodoModule = {
  components: {
    Todo,
  },
};

export default TodoModule;
