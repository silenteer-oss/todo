const translation = {
  moduleName: 'Quản lý công việc',
  inputTodoDetails: 'Nhập việc cần làm',
  selectPriority: 'Chọn độ ưu tiên',
  add: 'Thêm',
  remove: 'Xóa',
};

export default translation;

export type TTodoI18nKeys = keyof typeof translation;
