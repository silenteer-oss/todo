import React from 'react';
import ReactDOM from 'react-dom';
import { Switch, Redirect, Route } from 'react-router-dom';
import moment from 'moment';
import i18next from 'i18next';
import { lazyComponent } from '@design-system/core/components';
import AppSkeleton from '@infrastructure/components/AppSkeleton';
import Theme from '@todo/theme';
import {
  startBFFCommonSocket,
  stopBFFCommonSocket,
} from '@infrastructure/websocket';

const LazyTodoPage = lazyComponent('@todo/composer/page_todo/index.js');
const LazyLoginPage = lazyComponent('@todo/composer/page_login/index.js');

const theme = Theme.instance;
const globalStyle = {
  main: Theme.mixGlobalStyle(theme),
  blueprint: Theme.mixCustomBlueprintStyle(theme),
};

ReactDOM.render(
  <AppSkeleton
    theme={theme}
    globalStyle={globalStyle}
    onInitedI18n={() => moment.locale(i18next.language)}
    onChagedLanguage={(newLanguage) => moment.locale(newLanguage)}
    onDidMount={() => startBFFCommonSocket()}
    onWillUnmount={() => stopBFFCommonSocket()}
  >
    {() => (
      <Switch>
        <Route path="/todo" component={LazyTodoPage} />
        <Route path="/login" component={LazyLoginPage} />
        <Redirect exact path="/" to="/login" />
      </Switch>
    )}
  </AppSkeleton>,
  document.getElementById('root')
);
