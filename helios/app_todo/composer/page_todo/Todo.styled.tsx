import { ComponentType } from 'react';
import Theme, { ITodoStyledComponent } from '@todo/theme';
import OriginalTodo, { ITodoProps } from './Todo';

const Todo: ITodoStyledComponent<ComponentType<ITodoProps>> = Theme.styled(
  OriginalTodo
)`
  ${(props) => {
    const { theme } = props;

    return `
    & {
      width: 100%;
      height: 100%;
      padding-top: 32px;
      min-width: 1024px;
    }

    .sl-todo-page-header-total {
      margin-left: 8px;
      background-color: ${theme.background.warn.base};
      border-radius: 4px;
      color: ${theme.foreground.white};
      padding: 0 4px;
    }

    .sl-todo-page-content {
      min-width: 50%;
      max-width: 80%;
    }
    `;
  }}
`;

export default Todo;
