import { Box, Flex, H1 } from '@design-system/core/components';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { getImageUrl } from '@infrastructure/static-url';
import TodoModule from '@todo/module_todo';
import TodoReportModule from '@todo/module_todo-report';
import { ITodoThemeProps } from '@todo/theme';
import React, { memo } from 'react';
import { TTodoI18nKeys } from './Todo.i18n.vi';

const LogoSVG = getImageUrl('logo.svg');

export interface ITodoProps {}

function Todo(
  props: ITodoProps &
    ITodoThemeProps &
    II18nFixedNamespaceContext<TTodoI18nKeys>
) {
  const TodoComponent = TodoModule.components.Todo;
  const TodoReportComponent = TodoReportModule.components.TodoReport;

  const { t } = props;

  return (
    <Flex className={props.className} align="center" column auto>
      <Flex justify="center" align="center" column>
        <img src={LogoSVG} />
        <br />
        <br />
        <Flex justify="center" align="center">
          <H1>{t('moduleName')}</H1>
          <Box className="sl-todo-page-header-total">
            <TodoReportComponent />
          </Box>
        </Flex>
        <br />
        <br />
      </Flex>
      <Box className="sl-todo-page-content">
        <TodoComponent />
      </Box>
    </Flex>
  );
}

export default memo(
  withI18nContext(Todo, {
    namespace: '@todo/composer/page_todo/Todo',
  })
);
