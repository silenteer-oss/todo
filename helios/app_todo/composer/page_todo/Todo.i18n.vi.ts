const translation = {
  moduleName: 'Công việc cần làm',
};

export default translation;

export type TTodoI18nKeys = keyof typeof translation;
