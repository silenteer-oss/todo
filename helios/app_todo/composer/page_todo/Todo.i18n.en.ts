import { TTodoI18nKeys } from './Todo.i18n.vi';

const translation: { [key in TTodoI18nKeys]: string } = {
  moduleName: 'Todo',
};

export default translation;
