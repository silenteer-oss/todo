import { Flex } from '@design-system/core/components';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import LoginModule from '@todo/module_login';
import { ITodoThemeProps } from '@todo/theme';
import React, { memo } from 'react';
import { TLoginI18nKeys } from './Login.i18n.vi';

export interface ILoginProps {}

function Login(
  props: ILoginProps &
    ITodoThemeProps &
    II18nFixedNamespaceContext<TLoginI18nKeys>
) {
  const LoginComponent = LoginModule.components.Login;
  console.log({ props });

  return (
    <Flex className={props.className} auto>
      <LoginComponent onLoginSuccess={() => (window.location.href = '/todo')} />
    </Flex>
  );
}

export default memo(
  withI18nContext(Login, {
    namespace: '@todo/composer/page_login/Login',
  })
);
