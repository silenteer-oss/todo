import { TLoginI18nKeys } from './Login.i18n.vi';

const translation: { [key in TLoginI18nKeys]: string } = {};

export default translation;
