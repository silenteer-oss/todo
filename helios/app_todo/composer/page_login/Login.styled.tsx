import { ComponentType } from 'react';
import Theme, { ITodoStyledComponent } from '@todo/theme';
import OriginalLogin, { ILoginProps } from './Login';

const Login: ITodoStyledComponent<ComponentType<ILoginProps>> = Theme.styled(
  OriginalLogin
)`
  ${(props) => {
    const { theme } = props;

    return `
    & {
      width: 100%;
      height: 100%;
    }
    `;
  }}
`;

export default Login;
