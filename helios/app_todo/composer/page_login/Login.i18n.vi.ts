const translation = {};

export default translation;

export type TLoginI18nKeys = keyof typeof translation;
