import React from 'react';
import i18next, { TFunction } from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import { SentryService } from '@design-system/core/components';
import {
  II18nContext,
  II18nContextProviderProps,
  II18nContextProviderState,
} from './models';
import { I18nContext } from './I18nContext';

export class I18nContextProvider extends React.PureComponent<
  II18nContextProviderProps,
  II18nContextProviderState
> {
  private _i18nContext: II18nContext;
  state: II18nContextProviderState = {
    isReady: false,
  };

  constructor(props: II18nContextProviderProps) {
    super(props);

    if (!window['System']) {
      throw new Error(`systemjs isn't availabe`);
    }

    this._i18nContext = {
      currentLanguage: undefined,
      changeLanguage: this._changeLanguage,
      loadNamespace: this._loadNamespace,
    };
  }

  async componentDidMount() {
    const { onInitedI18n } = this.props;

    await this._init();
    this._i18nContext = {
      ...this._i18nContext,
      currentLanguage: i18next.language,
    };

    this.setState(
      {
        isReady: true,
      },
      () => {
        if (onInitedI18n) {
          onInitedI18n();
        }
      }
    );
  }

  render() {
    const { isReady } = this.state;
    return (
      <I18nContext.Provider value={this._i18nContext}>
        {isReady && this.props.children}
      </I18nContext.Provider>
    );
  }

  private _changeLanguage = (lng: string): Promise<void> => {
    if (lng === i18next.language) {
      return Promise.resolve();
    }

    const { onChangedLanguage } = this.props;

    return new Promise((resolve, reject) => {
      i18next
        .changeLanguage(lng)
        .then(() => {
          if (onChangedLanguage) {
            onChangedLanguage(lng);
          }

          this._i18nContext = { ...this._i18nContext, currentLanguage: lng };
          this.forceUpdate(() => resolve());
        })
        .catch((error) => {
          console.error(error);
          SentryService.captureException(error);
          reject(error);
        });
    });
  };

  private _loadNamespace = (namespace: string): Promise<void> => {
    const _lang = i18next.language;
    const _url = `${namespace}.i18n.${_lang}.js`;

    return window['System'].import(_url).then((source) => {
      i18next.addResourceBundle(
        _lang,
        namespace,
        source['default'] || source,
        true,
        true
      );
    });
  };

  private _init = (): Promise<TFunction> => {
    const {
      whitelist,
      fallbackLng,
      lng,
      ns = '',
      defaultNS = '',
      fallbackNS = '',
      debug,
      modules,
    } = this.props;

    const _languageDetector = new LanguageDetector(null, {
      order: ['querystring', 'readOnlyLocalStorage'],
    });

    _languageDetector.addDetector({
      name: 'readOnlyLocalStorage',
      lookup: (options) => {
        let found = 'vi';

        if (options.lookupLocalStorage) {
          const lng = window.localStorage.getItem(options.lookupLocalStorage);
          if (lng) found = lng;
        }

        return found;
      },
    });

    const result = i18next.use(_languageDetector);

    if (modules) {
      modules.forEach((i18nModule) => result.use(i18nModule));
    }

    return result.init({
      whitelist,
      fallbackLng,
      lng,
      ns,
      defaultNS,
      fallbackNS,
      debug,
      parseMissingKeyHandler: (key) => {
        return `__${key}__`;
      },
    });
  };
}
