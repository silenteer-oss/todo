import { Module, Newable, ThirdPartyModule, FallbackLngObjList } from 'i18next';

export interface II18nContext {
  currentLanguage: string;
  changeLanguage: (lng: string) => Promise<void>;
  loadNamespace: (namespace: string) => Promise<void>;
}

export interface II18nFixedNamespaceContext<T = string> extends II18nContext {
  t: (key: T, options?: { [key: string]: any }) => string;
}

export interface II18nContextProviderProps {
  whitelist: string[] | false;
  fallbackLng: string | false | string[] | FallbackLngObjList;
  lng?: string;
  ns?: string | string[];
  defaultNS?: string;
  fallbackNS?: string | string[] | false;
  debug?: boolean;
  modules?: Array<
    | Module
    | Newable<Module>
    | ThirdPartyModule[]
    | Array<Newable<ThirdPartyModule>>
  >;
  onInitedI18n?: () => void;
  onChangedLanguage?: (newLanguage: string) => void;
}

export interface II18nContextProviderState {
  isReady: boolean;
}
