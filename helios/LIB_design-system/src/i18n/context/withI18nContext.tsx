/* eslint-disable no-undef */
import React, {
  ReactNode,
  PureComponent,
  ComponentType,
  FunctionComponent,
} from 'react';
import i18next, { TFunction } from 'i18next';
import { II18nContext, II18nFixedNamespaceContext } from './models';
import { I18nContext } from './I18nContext';

interface I18nContainerProps {
  namespace: string;
  fallback?: ReactNode;
  children: (t: TFunction) => ReactNode;
}

interface I18nContainerState {
  isReady: boolean;
}

export function withI18nContext<TProps>(
  Component: ComponentType<TProps & II18nFixedNamespaceContext>,
  options: {
    namespace: string;
    fallback?: ReactNode;
  }
): FunctionComponent<TProps> {
  class I18nContainer extends PureComponent<
    I18nContainerProps & II18nContext,
    I18nContainerState
  > {
    private _t: TFunction;
    state: I18nContainerState = {
      isReady: false,
    };

    componentDidMount() {
      this._loadNamespace().then(() => this.setState({ isReady: true }));
    }

    componentDidUpdate(prevProps: I18nContainerProps & II18nContext) {
      if (this.props.currentLanguage !== prevProps.currentLanguage) {
        this.setState({ isReady: false }, () => {
          this._loadNamespace().then(() => this.setState({ isReady: true }));
        });
      }
    }

    render() {
      const { fallback } = this.props;
      const { isReady } = this.state;

      if (!isReady) {
        return fallback || null;
      }

      return this.props.children(this._t);
    }

    private _loadNamespace = (): Promise<void> => {
      const { namespace, loadNamespace } = this.props;

      return loadNamespace(namespace)
        .then(() => {
          this._t = i18next.getFixedT(i18next.language, namespace);
        })
        .catch((error) => {
          console.error(`${namespace}`, error);
          this._t = i18next.getFixedT(i18next.language, namespace);
        });
    };
  }

  return (props: TProps) => {
    return (
      <I18nContext.Consumer>
        {(contexts) => (
          <I18nContainer {...options} {...contexts}>
            {(t) => <Component {...props} {...contexts} t={t} />}
          </I18nContainer>
        )}
      </I18nContext.Consumer>
    );
  };
}
