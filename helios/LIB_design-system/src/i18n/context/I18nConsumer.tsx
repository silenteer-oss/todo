import React, { ReactNode } from 'react';
import { II18nContext } from './models';
import { I18nContext } from './I18nContext';

export class I18nContextConsumer extends React.PureComponent<{
  children: (value: II18nContext) => ReactNode;
}> {
  render() {
    return <I18nContext.Consumer>{this.props.children}</I18nContext.Consumer>;
  }
}
