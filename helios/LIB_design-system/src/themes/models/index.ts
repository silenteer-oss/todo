import { ComponentClass, ExoticComponent, ConsumerProps } from 'react';
import originalStyled, {
  keyframes,
  css as originalCss,
  createGlobalStyle as originalCreateGlobalStyle,
  withTheme as originalWithTheme,
  ThemeProvider as OriginalThemeProvider,
  ThemeConsumer as OriginalThemeConsumer,
  ThemedBaseStyledInterface,
  BaseThemedCssFunction,
  BaseWithThemeFnInterface,
  ThemeProviderProps,
  ThemedStyledProps,
  Interpolation,
  CSSObject,
  InterpolationFunction,
  GlobalStyleComponent,
} from 'styled-components';
import {
  ICoreTheme,
  ICoreThemeTypography,
  ICoreThemeSpace,
  ICoreThemeBreakpoint,
  ICoreThemeOpacity,
  ICoreThemeRadius,
  ICoreComponentsTheme,
} from '@design-system/core/models';

export interface IMicroFrontendTheme<TViewsTheme> extends ICoreTheme {
  views: TViewsTheme;
  setViewTheme(
    ...funcs: Array<(theme: IMicroFrontendTheme<TViewsTheme>) => TViewsTheme>
  ): void;
  setComponentTheme(
    ...funcs: Array<
      (theme: IMicroFrontendTheme<TViewsTheme>) => ICoreComponentsTheme
    >
  ): void;
}

export abstract class BaseMicroFrontendTheme<TViewsTheme>
  implements IMicroFrontendTheme<TViewsTheme> {
  abstract id: string;
  abstract isLightTheme: boolean;
  abstract isDarkTheme: boolean;
  abstract typography: ICoreThemeTypography;
  abstract space: ICoreThemeSpace;
  abstract breakpoint: ICoreThemeBreakpoint;
  abstract opacity: ICoreThemeOpacity;
  abstract radius: ICoreThemeRadius;
  components: ICoreComponentsTheme = {};
  views = {} as TViewsTheme;

  setComponentTheme(
    ...funcs: Array<
      (theme: IMicroFrontendTheme<TViewsTheme>) => ICoreComponentsTheme
    >
  ): void {
    this.components = {
      ...this.components,
      ...funcs.reduce(
        (result, func) => ({
          ...result,
          ...func(this),
        }),
        {} as ICoreComponentsTheme
      ),
    };
  }

  setViewTheme(
    ...funcs: Array<(theme: IMicroFrontendTheme<TViewsTheme>) => TViewsTheme>
  ): void {
    this.views = {
      ...this.views,
      ...funcs.reduce(
        (result, func) => ({
          ...result,
          ...func(this),
        }),
        {} as TViewsTheme
      ),
    };
  }
}

export function getConcreteStyledComponent<TTheme extends object>() {
  const styled = originalStyled as ThemedBaseStyledInterface<TTheme>;
  const css = originalCss as BaseThemedCssFunction<TTheme>;
  const createGlobalStyle = originalCreateGlobalStyle as <
    P extends object = {}
  >(
    first:
      | TemplateStringsArray
      | CSSObject
      | InterpolationFunction<ThemedStyledProps<P, TTheme>>,
    ...interpolations: Array<Interpolation<ThemedStyledProps<P, TTheme>>>
  ) => GlobalStyleComponent<P, TTheme>;
  const wrap = originalWithTheme as BaseWithThemeFnInterface<TTheme>;
  const Provider = OriginalThemeProvider as ComponentClass<
    ThemeProviderProps<TTheme, TTheme>,
    any
  >;
  const Consumer = OriginalThemeConsumer as ExoticComponent<
    ConsumerProps<TTheme>
  >;

  return {
    Provider,
    Consumer,
    wrap,
    styled,
    css,
    createGlobalStyle,
    keyframes,
  };
}
