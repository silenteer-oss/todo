export interface IGlobalStyleContext {
  putGlobalStyle: (id: string, style: string) => void;
  addGlobalStyle: (id: string, style: string) => void;
}

export interface IGlobalStyleContextProviderProps {
  styles?: { [key: string]: string };
}

export interface IGlobalStyleContextProviderState {
  styleMap: { [key: string]: string };
}
