import React, { ComponentType, FunctionComponent } from 'react';
import { IGlobalStyleContext } from './models';
import { GlobalStyleContext } from './GlobalStyleContext';

export function withGlobalStyleContext<TProps>(
  Component: ComponentType<TProps & IGlobalStyleContext>
): FunctionComponent<TProps> {
  return (props: TProps) => {
    return (
      <GlobalStyleContext.Consumer>
        {(contexts) => <Component {...props} {...contexts} />}
      </GlobalStyleContext.Consumer>
    );
  };
}
