export function getDateInfo(
  target: Date
): {
  date: number;
  day: number;
  month: number;
  year: number;
  hours: number;
  minutes: number;
  seconds: number;
  milliseconds: number;
} {
  return {
    date: target.getDate(),
    day: target.getDay(),
    month: target.getMonth(),
    year: target.getFullYear(),
    hours: target.getHours(),
    minutes: target.getMinutes(),
    seconds: target.getSeconds(),
    milliseconds: target.getMilliseconds(),
  };
}

export function equalDate(
  date1: Date,
  date2: Date,
  ignoreTime?: boolean
): boolean {
  if (!date1 && !date2) {
    return true;
  } else if (!date1 || !date2) {
    return false;
  }

  if (ignoreTime) {
    return (
      getDateWithoutTime(date1).getTime() ===
      getDateWithoutTime(date2).getTime()
    );
  } else {
    return date1.getTime() === date2.getTime();
  }
}

export function getFirstDateOfWeek(date: Date): Date {
  const day = date.getDay();
  return new Date(
    date.getFullYear(),
    date.getMonth(),
    date.getDate() + (day == 0 ? -6 : 1) - day
  );
}

export function getLastDateOfWeek(date: Date): Date {
  const day = date.getDay();
  return new Date(
    date.getFullYear(),
    date.getMonth(),
    date.getDate() + (day == 0 ? 0 : 7) - day
  );
}

export function getFirstDateOfMonth(date: Date): Date {
  return new Date(date.getFullYear(), date.getMonth(), 1);
}

export function getLastDateOfMonth(date: Date): Date {
  return new Date(date.getFullYear(), date.getMonth() + 1, 0);
}

export function getFirstDateOfYear(date: Date): Date {
  return new Date(date.getFullYear(), 0, 1);
}

export function getLastDateOfYear(date: Date): Date {
  return new Date(date.getFullYear(), 11, 31);
}

export function getDaysInMonth(month: number, year: number): number {
  return new Date(year, month, 0).getDate();
}

export function getDateWithoutTime(target?: string | number | Date): Date {
  const result = target ? new Date(target) : new Date();
  result.setHours(0, 0, 0, 0);

  return result;
}

export function setEndOfDate(target?: string | number | Date): Date {
  const result = target ? new Date(target) : new Date();
  result.setHours(23, 59, 59, 0);

  return result;
}

export function getDateWithTime(target?: string | number | Date): Date {
  const result = target ? new Date(target) : new Date();
  return result;
}

export function addHours(target: Date, hours: number): Date {
  const _date = new Date(target);
  return new Date(_date.setHours(_date.getHours() + hours));
}

export function addMinutes(target: Date, minutes: number): Date {
  const _date = new Date(target);
  return new Date(_date.setMinutes(_date.getMinutes() + minutes));
}

export function addDay(target: Date, days: number): Date {
  const _date = new Date(target);
  return new Date(_date.setDate(_date.getDate() + days));
}

export function addWeek(target: Date, weeks: number): Date {
  const _date = new Date(target);
  return new Date(
    _date.setDate(
      getFirstDateOfWeek(_date).getDate() + 6 * weeks + (weeks > 0 ? 1 : -1)
    )
  );
}

export function addMonth(
  target: Date,
  months: number,
  dayOfMonth?: number
): Date {
  const _date = new Date(target);
  return new Date(_date.setMonth(_date.getMonth() + months, dayOfMonth));
}

export function subtractMonth(target: Date, months: number): Date {
  const _date = new Date(target);
  return new Date(_date.setMonth(_date.getMonth() - months));
}

export function addYear(
  target: Date,
  years: number,
  monthOfYear?: number,
  dayOfMonth?: number
): Date {
  const _date = new Date(target);
  return new Date(
    _date.setFullYear(
      _date.getFullYear() + years,
      monthOfYear || _date.getMonth(),
      dayOfMonth || _date.getDate()
    )
  );
}

export function differentDays(date1: Date, date2: Date): number {
  return Math.round(
    Math.abs((date1.getTime() - date2.getTime()) / (24 * 60 * 60 * 1000))
  );
}
