import React, { Suspense } from 'react';

export function lazyComponent<T>(
  url: string,
  fallback?: React.ReactElement
): React.FunctionComponent<T> {
  const Component = React.lazy(() =>
    window['System'].import(url)
  ) as React.LazyExoticComponent<React.ComponentType<any>>;
  return (props) => (
    <Suspense fallback={fallback || <div>Loading...</div>}>
      <Component {...props} />
    </Suspense>
  );
}
