import { ReactElement } from 'react';
import { ICoreTheme } from '../../../models';

export interface ISvgProps {
  className?: string;
  theme: ICoreTheme;
  src?: string;
  element?: ReactElement;
  w?: number;
  h?: number;
  size?: number;
}
