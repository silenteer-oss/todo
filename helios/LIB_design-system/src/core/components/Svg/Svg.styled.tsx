import { getCssClass } from '@design-system/infrastructure/utils';
import { styled } from '../../models';
import { ISvgProps } from './models';
import { OriginSvg } from './Svg';

export const Svg = styled(OriginSvg).attrs(({ className }) => ({
  className: getCssClass('sl-Svg', className),
}))<ISvgProps>`
  svg {
    ${({ w }) => w && `width: ${w}px`};
    ${({ h }) => h && `height: ${h}px`};
    ${({ size }) => size && `height: ${size}px; width: ${size}px`};
    ${({ fill }) => fill && `fill: ${fill}`};
  }
`;
