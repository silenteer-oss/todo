import React, { ComponentType } from 'react';

// eslint-disable-next-line prettier/prettier
export interface IMicroModules<TModule1, TModule2=undefined, TModule3=undefined, TModule4=undefined, TModule5=undefined, TModule6=undefined, TModule7=undefined, TModule8=undefined, TModule9=undefined, TModule10=undefined> {
  // eslint-disable-next-line prettier/prettier
  modules: [TModule1, TModule2, TModule3, TModule4, TModule5, TModule6, TModule7, TModule8, TModule9, TModule10];
}

// eslint-disable-next-line prettier/prettier
export function withMicroModule<TProps, TModule1, TModule2=undefined, TModule3=undefined, TModule4=undefined, TModule5=undefined, TModule6=undefined, TModule7=undefined, TModule8=undefined, TModule9=undefined, TModule10=undefined>(
  Component: ComponentType<
    // eslint-disable-next-line prettier/prettier
    TProps & IMicroModules<TModule1, TModule2, TModule3, TModule4, TModule5, TModule6, TModule7, TModule8, TModule9, TModule10>
  >,
  options: {
    urls: string[];
  }
): React.ComponentClass<TProps> {
  class Container extends React.PureComponent<
    TProps,
    // eslint-disable-next-line prettier/prettier
    IMicroModules<TModule1, TModule2, TModule3, TModule4, TModule5, TModule6, TModule7, TModule8, TModule9, TModule10>
  > {
    componentDidMount() {
      // eslint-disable-next-line prettier/prettier
      _loadMicroModule<TModule1, TModule2, TModule3, TModule4, TModule5, TModule6, TModule7, TModule8, TModule9, TModule10>(options.urls)
        .then((results) =>
          this.setState({
            modules: results,
          })
        )
        .catch((err) => {
          throw err;
        });
    }

    render() {
      return <Component {...this.props} {...this.state} />;
    }
  }

  return Container;
}

// --------------------------------------------------------------------------------- //

// eslint-disable-next-line prettier/prettier
function _loadMicroModule<TModule1, TModule2=undefined, TModule3=undefined, TModule4=undefined, TModule5=undefined, TModule6=undefined, TModule7=undefined, TModule8=undefined, TModule9=undefined, TModule10=undefined>(
  urls: string[]
): // eslint-disable-next-line prettier/prettier
Promise<[TModule1, TModule2, TModule3, TModule4, TModule5, TModule6, TModule7, TModule8, TModule9, TModule10]> {
  if (!window['System']) {
    throw new Error(`systemjs isn't availabe`);
  }

  // eslint-disable-next-line prettier/prettier
  return Promise.all<TModule1, TModule2, TModule3, TModule4, TModule5, TModule6, TModule7, TModule8, TModule9, TModule10>(
    urls.map((url) =>
      window['System']
        .import(url)
        .then((microModule) => {
          return microModule && microModule['default']
            ? microModule['default']
            : microModule;
        })
        .catch((error) => {
          console.error(error);
          return undefined;
        })
    ) as any
  );
}
