import * as Sentry from '@sentry/browser';

// ignore Sentry in develop or e2e mode
const ignoreSentry = process.env.NODE_ENV === 'develop' || window['Cypress'];

const setUserContext = (user = {}) => {
  Sentry.configureScope((scope) => {
    scope.setUser(user);
  });
};

const captureMessage = (message: string, extra) => {
  if (ignoreSentry) {
    return;
  }
  Sentry.setExtras(extra);
  Sentry.captureMessage(message);
  return Sentry.lastEventId();
};

const captureException = (exception) => {
  if (ignoreSentry) {
    return;
  }
  Sentry.captureException(exception);
  return Sentry.lastEventId();
};

const initService = ({ sentry, user }, beforeSend?: any) => {
  if (!sentry || ignoreSentry) {
    return;
  }

  Sentry.init({
    dsn: sentry.dsn,
    release: process.env.BUILD_NUMBER, //TODO:
    environment: document.location.hostname,
    beforeSend(event) {
      beforeSend && beforeSend(event);
      return event;
    },
  });
  setUserContext(user);
};

export const SentryService = {
  initService,
  captureMessage,
  captureException,
  setUserContext,
};
