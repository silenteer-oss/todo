import React from 'react';
import { Flex } from '../Flexbox/Flex.styled';
import { ILoadingStateProps } from './LoadingStateModel';

export class OriginLoadingState extends React.PureComponent<
  ILoadingStateProps
> {
  render() {
    const { className } = this.props;

    return (
      <Flex align="center" justify="center" className={className}>
        <div className="loader" />
      </Flex>
    );
  }
}
