Helios contains setup to share lint, perttier, node modules for all LIB_* & app_*
So if you need to install new node module while working on LIB_* or app_*, install it to Helios.

*Note that* if LIB_* or app_* in your local contains any node_modules/yarn.lock, please remove them totally to abvoid unexpected error in build time.

To install node modules for all packages before working, you should run
```
  npm run init
```

Then go to `apollo` project to build & start dev server(Read README.md in apollo to get detail)

<!-- ----------------------------------------------------------------- -->

You can get more detail about standard structure which is applied to this project at
https://www.notion.so/silenteer/RFD-Standard-Structure-for-Frontend-Project-03fc9672284d4e93ab7f9dc1a5f3afd5

You can get more detail about best practice should follow when work on this project at
https://www.notion.so/silenteer/RFD-Best-Practices-for-Frontend-836dae65a04b4ef486911fe85bf6b8a0