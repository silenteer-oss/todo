import { SignalClient } from '@test/signal-client';

describe('Signal', () => {
  it('could decrypt the encrypted message', async () => {
    const alice = new SignalClient('alice');
    const bob = new SignalClient('bob');

    const keyDistribution = await alice.createKeyDistributionTo(bob);
    await bob.processKeyDistributionFrom(alice, keyDistribution);

    const encryptedChatMessage = await alice.encryptMessageToSendTo(
      bob,
      'chúc mừng sinh nhật yê Ä/ä, Ö/ö, Ü/ü ß'
    );
    const decryptedMessage = await bob.decryptMessageFrom(
      alice,
      encryptedChatMessage
    );

    expect(decryptedMessage).to.equal('chúc mừng sinh nhật yê Ä/ä, Ö/ö, Ü/ü ß');
  });

  it('could decrypt out-of-order messages', async () => {
    const alice = new SignalClient('alice');
    const bob = new SignalClient('bob');

    const keyDistribution = await alice.createKeyDistributionTo(bob);
    await bob.processKeyDistributionFrom(alice, keyDistribution);

    const encryptedChatMessage1 = await alice.encryptMessageToSendTo(
      bob,
      'tin nhắn 1'
    );
    const encryptedChatMessage2 = await alice.encryptMessageToSendTo(
      bob,
      'tin nhắn 2'
    );
    const encryptedChatMessage3 = await alice.encryptMessageToSendTo(
      bob,
      'tin nhắn 3'
    );

    const decryptedMessage1 = await bob.decryptMessageFrom(
      alice,
      encryptedChatMessage1
    );
    const decryptedMessage3 = await bob.decryptMessageFrom(
      alice,
      encryptedChatMessage3
    );
    const decryptedMessage2 = await bob.decryptMessageFrom(
      alice,
      encryptedChatMessage2
    );

    expect(decryptedMessage1).to.equal('tin nhắn 1');
    expect(decryptedMessage2).to.equal('tin nhắn 2');
    expect(decryptedMessage3).to.equal('tin nhắn 3');
  });
});
