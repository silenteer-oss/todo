export default {
  unsupportBrowserTitle: 'Trình duyệt không được hỗ trợ',
  unsupportBrowserContent:
    'Để sử dụng được hết tất cả các tính năng của ứng dụng, bạn nên dùng phiên bản mới nhất của trình duyệt Chrome hoặc Firefox',
  deniedPermissionTitle: 'Ứng dụng cần được cấp quyền',
  deniedPermissionContent:
    'Bạn hãy tải lại ứng dụng chấp nhận quyền được yêu cầu từ hộp thoại trên trình duyệt.',
};
