export { default } from './AppSkeleton';
export {
  IAppSkeletonProps,
  IAppSkeletonState,
  II18nSettingProps,
} from './AppSkeleton';
