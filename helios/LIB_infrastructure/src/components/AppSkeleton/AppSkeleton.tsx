import React, { memo } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Dialog, Toaster, Popover } from '@blueprintjs/core';
import { ThemeProvider } from 'styled-components';
import {
  LoadingState,
  ErrorBoundary,
  EnsureSupportedBrowser,
} from '@design-system/core/components';
import { GlobalStyleContextProvider } from '@design-system/themes/context';
import { I18nContextProvider } from '@design-system/i18n/context';
import {
  II18nFixedNamespaceContext,
  withI18nContext,
} from '@design-system/i18n/context';
import { DEFAULT_LANGUAGE, SUPPORTED_LANGUAGES } from '@infrastructure/i18n';
import { setupBrowser } from '@infrastructure/browser';

// This code is used to solve issue here https://github.com/palantir/blueprint/issues/394
Popover.defaultProps.modifiers = { computeStyle: { gpuAcceleration: false } };

export interface IAppSkeletonProps {
  theme: any;
  children: (
    state: IAppSkeletonState,
    apiErrorRedirect: { [status: number]: string }
  ) => React.ReactNode;
  apiErrorRedirect?: { [status: number]: string };
  globalStyle?: {
    [key: string]: string;
  };
  onDidMount?: () => void;
  onWillUnmount?: () => void;
  onGetInitial?: () => Promise<{
    status?: number;
    user?: any;
    configs?: {
      [key: string]: object;
    };
  }>;
}

export interface IAppSkeletonState {
  status?: number;
  user?: any;
  configs?: {
    [key: string]: object;
  };
}

export interface II18nSettingProps {
  onInitedI18n?: () => void;
  onChagedLanguage?: (newLanguage: string) => void;
}

interface II18nSettingState {
  isInitedI18n?: boolean;
}

class I18nSetting extends React.PureComponent<
  II18nSettingProps,
  II18nSettingState
> {
  private _i18nInitOption = {
    lng: DEFAULT_LANGUAGE,
    whitelist: SUPPORTED_LANGUAGES,
    fallbackLng: DEFAULT_LANGUAGE,
    debug: process.env.NODE_ENV === 'develop',
  };
  state: II18nSettingState = {};

  render() {
    const { children } = this.props;
    const { isInitedI18n } = this.state;

    return (
      <I18nContextProvider
        {...this._i18nInitOption}
        onInitedI18n={this._handleInitedI18n}
        onChangedLanguage={this._handleChagedLanguage}
      >
        {isInitedI18n && children}
      </I18nContextProvider>
    );
  }

  private _handleInitedI18n = () => {
    const { onInitedI18n } = this.props;

    this.setState({ isInitedI18n: true });

    if (onInitedI18n) {
      onInitedI18n();
    }
  };

  private _handleChagedLanguage = (newLanguage: string) => {
    const { onChagedLanguage } = this.props;

    if (onChagedLanguage) {
      onChagedLanguage(newLanguage);
    }
  };
}

class OriginalAppSkeleton extends React.PureComponent<
  IAppSkeletonProps & II18nFixedNamespaceContext,
  IAppSkeletonState
> {
  private _defaultApiErrorRedirect = {
    401: '/login',
    403: '/login',
  };
  state: IAppSkeletonState = {};

  constructor(props: IAppSkeletonProps & II18nFixedNamespaceContext) {
    super(props);

    const { onGetInitial } = props;
    if (onGetInitial) {
      onGetInitial().then((data) => this.setState({ ...data }));
    }
  }

  componentDidMount() {
    const { onDidMount } = this.props;
    if (onDidMount) {
      onDidMount();
    }
  }

  componentWillUnmount() {
    const { onWillUnmount } = this.props;
    if (onWillUnmount) {
      onWillUnmount();
    }
  }

  render() {
    const { children, theme, globalStyle, t } = this.props;
    const { user, configs } = this.state;
    const noInitial = !this.props.onGetInitial;
    const apiErrorRedirect = this.props.apiErrorRedirect
      ? this.props.apiErrorRedirect
      : this._defaultApiErrorRedirect;

    return (
      <ThemeProvider theme={theme}>
        <GlobalStyleContextProvider styles={globalStyle}>
          {!noInitial && (!user || !configs) && <LoadingState />}
          {!noInitial && user && configs && (
            <BrowserRouter>
              <ErrorBoundary
                user={user}
                sentry={configs.sentry}
                Dialog={Dialog}
                Toaster={Toaster}
                apiErrorRedirect={apiErrorRedirect}
              >
                <EnsureSupportedBrowser
                  features={['indexDB', 'storageManager']}
                  unsupportedBrowserMessages={{
                    title: t('unsupportBrowserTitle'),
                    content: t('unsupportBrowserContent'),
                  }}
                  deniedPermissionMessages={{
                    title: t('deniedPermissionTitle'),
                    content: t('deniedPermissionContent'),
                  }}
                  onRequestPermission={setupBrowser}
                >
                  {children(this.state, apiErrorRedirect)}
                </EnsureSupportedBrowser>
              </ErrorBoundary>
            </BrowserRouter>
          )}
          {noInitial && (
            <BrowserRouter>
              {children(this.state, apiErrorRedirect)}
            </BrowserRouter>
          )}
        </GlobalStyleContextProvider>
      </ThemeProvider>
    );
  }
}

const AppSkeleton: React.MemoExoticComponent<(
  props: IAppSkeletonProps &
    II18nSettingProps & {
      children?: React.ReactNode;
    }
) => JSX.Element> = memo(
  (
    props: IAppSkeletonProps &
      II18nSettingProps & {
        children?: React.ReactNode;
      }
  ) => {
    const { children, onInitedI18n, onChagedLanguage, ...rest } = props;
    const AppSkeletonHOC = withI18nContext(OriginalAppSkeleton, {
      namespace: '@infrastructure/components/AppSkeleton/AppSkeleton',
    });
    return (
      <I18nSetting
        onInitedI18n={onInitedI18n}
        onChagedLanguage={onChagedLanguage}
      >
        <AppSkeletonHOC {...rest}>
          {(state, apiErrorRedirect) => children(state, apiErrorRedirect)}
        </AppSkeletonHOC>
      </I18nSetting>
    );
  }
);

export default AppSkeleton;
