export function getImageUrl(fileName: string): string {
  return `/assets/images/${fileName}`;
}

export function getStyleUrl(fileName: string): string {
  return `/assets/styles/${fileName}`;
}

export function getDataUrl(fileName: string): string {
  return `/assets/data/${fileName}`;
}

export function get3rdLibUrl(libName: string): string {
  return `/3rd-libs/${libName}`;
}
