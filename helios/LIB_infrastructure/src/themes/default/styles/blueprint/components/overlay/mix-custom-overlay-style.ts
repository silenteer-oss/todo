import { IDefaultTheme } from '../../../../theme';

export function mixCustomOverlayStyle<TViewsTheme>(
  theme: IDefaultTheme<TViewsTheme>
): string {
  return `
    .bp3-overlay {
      z-index: 20000 !important;

      .bp3-overlay-backdrop {
        z-index: 20000 !important;
      }
      .bp3-dialog-container {
        z-index: 20000 !important;
      }
      .bp3-transition-container {
        z-index: 20000 !important;
      }
    }
  `;
}
