import { scaleSpacePx } from '@design-system/core/styles';
import { IDefaultTheme } from '../../../../theme';

export function mixCustomDateRangePickerStyle<TViewsTheme>(
  theme: IDefaultTheme<TViewsTheme>
): string {
  const { background, foreground, space } = theme;

  return `
    .bp3-datepicker.bp3-daterangepicker {
      
      .bp3-datepicker-navbar {
        .bp3-button {
          min-width: 0;
          
          &:disabled {
            background: none;
          }
        }
      }
      
      
    }
  `;
}
