import {
  scaleSpace,
  scaleSpacePx,
  mixTypography,
} from '@design-system/core/styles';
import { IDefaultTheme } from '../../../../theme';

export function mixCustomTimePickerStyle<TViewsTheme>(
  theme: IDefaultTheme<TViewsTheme>
): string {
  const { background, typography } = theme;

  return `
    .bp3-timepicker {
      .bp3-timepicker-input-row {
        height: ${scaleSpacePx(9)};
        padding: 0;
      }
      .bp3-timepicker-input {
        width: ${scaleSpacePx(8)};
        height: ${scaleSpacePx(9)};

        &:focus {
          width: ${scaleSpace(8) - 2}px;
          height: ${scaleSpace(9) - 2}px;
          border: 1px solid ${background.second.base};
          box-shadow: none;
        }

        ${mixTypography('bodyM', {
          color: typography.bodyM.color,
          fontFamily: typography.bodyM.fontFamily,
          fontSize: typography.bodyM.fontSize,
          fontWeight: typography.bodyM.fontWeight,
          lineHeight: typography.bodyM.lineHeight,
          letterSpacing: typography.bodyM.letterSpacing,
        })}
      }
    }
  `;
}
