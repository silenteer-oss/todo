export * from './mix-custom-select-style';

export const SELECT_FULLWIDTH_CLASS = 'sl-select full-width';
export const SELECT_POPOVER_FULLWIDTH_PROPS = {
  minimal: true,
  usePortal: false,
  fill: true,
  modifiers: { computeStyle: { enabled: false } },
};
