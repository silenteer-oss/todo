import { scaleSpacePx, mixTypography } from '@design-system/core/styles';
import { IDefaultTheme } from '../../../../theme';

export function mixCustomTagInputStyle<TViewsTheme>(
  theme: IDefaultTheme<TViewsTheme>
): string {
  const { space, typography, background, foreground } = theme;

  return `
    .bp3-tag-input {
      min-height: ${scaleSpacePx(5)};

      &:not([class*=bp3-intent-]) {
        &:focus,
        &.bp3-active {
          border: 1px solid ${background.primary.base};
          box-shadow: none;
        }
      }

      &.bp3-intent-primary,
      &.bp3-intent-primary:focus,
      &.bp3-intent-primary.bp3-active {
        border: 1px solid ${background.primary.base};
        box-shadow: none;
      }

      &.bp3-intent-success,
      &.bp3-intent-success:focus,
      &.bp3-intent-success.bp3-active {
        border: 1px solid ${background.success.base};
        box-shadow: none;
      }

      &.bp3-intent-warning,
      &.bp3-intent-warning:focus,
      &.bp3-intent-warning.bp3-active {
        border: 1px solid ${background.warn.base};
        box-shadow: none;
      }

      &.bp3-intent-danger,
      &.bp3-intent-danger:focus,
      &.bp3-intent-danger.bp3-active {
        border: 1px solid ${background.error.base};
        box-shadow: none;
      }

      .bp3-tag-input-values {
        margin-top: 0;
        margin-right: 0;

        .bp3-input-ghost {
          padding-left: 0;
          margin-bottom: 0;

          ${mixTypography('bodyM', {
            color: typography.bodyM.color,
            fontFamily: typography.bodyM.fontFamily,
            fontSize: typography.bodyM.fontSize,
            fontWeight: typography.bodyM.fontWeight,
            lineHeight: typography.bodyM.lineHeight,
            letterSpacing: typography.bodyM.letterSpacing,
          })}
        }
      }
    }
  `;
}
