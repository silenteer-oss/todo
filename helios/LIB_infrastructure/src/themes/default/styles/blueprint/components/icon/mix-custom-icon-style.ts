import { IDefaultTheme } from '../../../../theme';

export function mixCustomIconStyle<TViewsTheme>(
  theme: IDefaultTheme<TViewsTheme>
): string {
  const { foreground } = theme;

  return `
    .bp3-icon {
      color: ${foreground['03']};

      &.bp3-intent-primary {
        color: ${foreground.primary.base};
      }
      &.bp3-intent-success {
        color: ${foreground.success.base};
      }
      &.bp3-intent-warning {
        color: ${foreground.warn.base};
      }
      &.bp3-intent-danger {
        color: ${foreground.error.base};
      }
    }
  `;
}
