import { scaleSpacePx } from '@design-system/core/styles';
import { IDefaultTheme } from '../../../../theme';

export function mixCustomToastStyle<TViewsTheme>(
  theme: IDefaultTheme<TViewsTheme>
): string {
  const { foreground, background, space } = theme;

  return `
    .bp3-toast {
      align-items: center;
      padding: ${space.xs} ${space.s} !important;
      box-shadow: 0px 8px 24px rgba(16, 22, 26, 0.2), 0px 2px 4px rgba(16, 22, 26, 0.2), 0px 0px 0px rgba(16, 22, 26, 0.1);

      &.bp3-intent-primary {
        background-color: ${background.primary.base};
      }
      &.bp3-intent-success {
        background-color: ${background.success.base};
      }
      &.bp3-intent-warning {
        background-color: ${background.warn.base};
      }
      &.bp3-intent-danger {
        background-color: ${background.error.base};
      }

      > .bp3-icon {
        color: ${foreground['01']};
        margin: 0;
      }

      .bp3-toast-message {
        padding: 0;
        margin: 0 ${space.xs};
      }

      > .bp3-button-group {
        padding: 0;

        &.bp3-minimal .bp3-button {
          min-width: initial;
          min-height: initial;
          padding: ${space.xxs} ${scaleSpacePx(3)};
          background: none;

          &:hover {
            background: none !important;
          }
        }
      }
    }
  `;
}
