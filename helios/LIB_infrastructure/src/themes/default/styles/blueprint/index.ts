import { IDefaultTheme } from '../../theme';
import {
  mixCustomAlertStyle,
  mixCustomButtonStyle,
  mixCustomDatePickerStyle,
  mixCustomDateRangePickerStyle,
  mixCustomTimePickerStyle,
  mixCustomDialogStyle,
  mixCustomDividerStyle,
  mixCustomIconStyle,
  mixCustomMenuStyle,
  mixCustomInputStyle,
  mixCustomInputGroupStyle,
  mixCustomControlsStyle,
  mixCustomFormGroupStyle,
  mixCustomHtmlTableStyle,
  mixCustomLabelStyle,
  mixCustomOverlayStyle,
  mixCustomPopoverStyle,
  mixCustomSelectStyle,
  mixCustomTagStyle,
  mixCustomTagInputStyle,
  mixCustomToastStyle,
} from './components';

export function mixCustomBlueprintStyle<TViewsTheme>(
  theme: IDefaultTheme<TViewsTheme>
): string {
  return `
    body {
      ${mixCustomAlertStyle(theme)}
      ${mixCustomButtonStyle(theme)}
      ${mixCustomDatePickerStyle(theme)}
      ${mixCustomDateRangePickerStyle(theme)}
      ${mixCustomTimePickerStyle(theme)}
      ${mixCustomDialogStyle(theme)}
      ${mixCustomDividerStyle(theme)}
      ${mixCustomIconStyle(theme)}
      ${mixCustomMenuStyle(theme)}
      ${mixCustomInputStyle(theme)}
      ${mixCustomInputGroupStyle(theme)}
      ${mixCustomControlsStyle(theme)}
      ${mixCustomFormGroupStyle(theme)}
      ${mixCustomHtmlTableStyle(theme)}
      ${mixCustomLabelStyle(theme)}
      ${mixCustomOverlayStyle(theme)}
      ${mixCustomPopoverStyle(theme)}
      ${mixCustomSelectStyle(theme)}
      ${mixCustomTagStyle(theme)}
      ${mixCustomTagInputStyle(theme)}
      ${mixCustomToastStyle(theme)}
    }
  `;
}
