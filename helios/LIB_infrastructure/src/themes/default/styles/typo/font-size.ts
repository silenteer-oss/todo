export const FONT_SIZE = {
  TYPO_FONT_SIZE_H1: '24px',
  TYPO_FONT_SIZE_H2: '20px',
  TYPO_FONT_SIZE_H3: '18px',
  TYPO_FONT_SIZE_H4: '16px',
  TYPO_FONT_SIZE_BODY_M: '14px',
  TYPO_FONT_SIZE_BODY_S: '12px',
  TYPO_FONT_SIZE_LINK: '14px',
};
