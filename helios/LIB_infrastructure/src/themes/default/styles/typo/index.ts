import { FONT_FAMILY } from './font-family';
import { FONT_SIZE } from './font-size';
import { FONT_WEIGHT } from './font-weight';
import { LINE_HEIGHT } from './line-height';

export { mixTypography } from './mixins';
export { getTextAlignClass } from './text-align';

export const TYPO = {
  ...FONT_FAMILY,
  ...FONT_SIZE,
  ...FONT_WEIGHT,
  ...LINE_HEIGHT,
};
