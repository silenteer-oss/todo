export * from '@silenteer/hermes/websocket';

export function getWsHostUrl(): string {
  if (!window) return undefined;

  const wsProtocol = window.location.protocol === 'https:' ? 'wss' : 'ws';
  return `${wsProtocol}://${window.location.host}`;
}

import {
  startBFFCommonSocket as _startBFFCommonSocket,
  createBFFPairingWs as _createBFFPairingWs,
} from '@silenteer/hermes/websocket';
export const startBFFCommonSocket = () => _startBFFCommonSocket(getWsHostUrl());
export const createBFFPairingWs = (pairingAddress: string) =>
  _createBFFPairingWs(getWsHostUrl(), pairingAddress);
